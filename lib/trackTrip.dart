import 'dart:convert';

import 'package:ao_courier_client/map_track.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'globals.dart' as globals;
import 'main.dart';

callSTK() async {

  String url = globals.BASE_URL;
  // String  print("RESPONSE DESCRIPTION === > $responseDescription");
  String resp = "", responseDescription = "", responseCode = "";
  var jsonObj;
 // SharedPreferences prefs = await SharedPreferences.getInstance();

  Map map = {
    'username': globals.username,
    'password': globals.password,
    "processingCode": "STK",
    "amount" : globals.newTrip_cost.substring(0,globals.newTrip_cost.indexOf(".")),
    "PhoneNumber": globals.phone,
    'channel': globals.channel,
  };

  resp = (await globals.apiRequest(url, map));
  print(resp);
  jsonObj = json.decode(resp);

  responseCode = jsonObj['responseCode'];
  responseDescription = jsonObj['responseDescription'];

 // Navigator.pop(context);
print("RESPONSE DESCRIPTION === > $responseDescription");
print("RESPONSE Code === > $responseCode");
 // Toast.show(responseDescription, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

  if (responseCode == "000") // success
      {
  }
  else // invalid details
      {

  }

}

class TrackTrip extends StatefulWidget {
  @override
  _TrackTripState createState() => _TrackTripState();
}

class _TrackTripState extends State<TrackTrip> {
  @override
  void initState() {
    super.initState();
    // _getCurrentLocation();


    Future.delayed(Duration(seconds: 20), () {
      print("Executed after 10 seconds");
      String msg = "";

      /* // !PAYMENT
      if (globals.tripDetails_pay_method == "CASH")
      {
        msg = "Please pay rider cash Ksh" + globals.tripDetails_cost;
      }
      else if (globals.tripDetails_pay_method == "MPESA")
      {
        msg = "Please complete Mpesa payment of Ksh" + globals.tripDetails_cost;
      }
      */

      showNotification(
          "Delivery Complete", msg);

      showMessage("Delivery Complete", msg, context);

    });
  }

  void showNotification(String title, String msg) {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      // _counter++;
    });
    flutterLocalNotificationsPlugin.show(
        globals.newPushID(),
        title,
        msg,
        NotificationDetails(
            android: AndroidNotificationDetails(
                channel.id, channel.name, channel.description,
                importance: Importance.high,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title:
                Text('Current Trip Details', style: TextStyle(color: Colors.black87)),
            backgroundColor: Colors.white,
            leading: GestureDetector(
              onTap: () {
                if (Navigator.canPop(context)) {
                  Navigator.pop(context);
                }
                /* else {
               SystemNavigator.pop();
             } */
              },
              child: Icon(Icons.arrow_back, color: Colors.black87),
            )),
        body: Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            // alignment: Alignment.topLeft,
            child: Column(children: [
              Align(
                  alignment: Alignment(0.0, 0.0),
                  child: Text(
                    'Estimated Arrival: 20mins',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  )),
              Divider(
                height: 15,
                color: Colors.transparent,
              ),
              // Align(
              //   alignment: Alignment(0.0, 0.0),
              // child: MapViewTrack())
            ])));
  }
}

void showMessage(String title, String message, BuildContext context)
{
  BuildContext dialogContext;
  showDialog(
    barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        dialogContext = context;
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Text(message)],
            ),
          ),
          actions: <Widget>[
            // TextButton(
            //   onPressed: () => Navigator.pop(context, 'Cancel'),
            //   child: const Text('Reject'),
            // ),
            TextButton(
              onPressed: () {
              //  acceptTrip(context);
                Navigator.pop(dialogContext);
                _showRatingAppDialog(context);
                globals.packageDetailsOK = false;
                globals.pageController.animateToPage(0,
                    duration: Duration(milliseconds: 500), curve: Curves.easeInCubic);

                // ! CALL STK
                // if (globals.newTrip_pay_method == "MPESA")
                //   {
                //     callSTK();
                //   }
          //      Navigator.pop(context, 'Cancel');
              //  Navigator.of(context, rootNavigator: true).pop();

              },
              /* Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (_) => LandingPage())
              ), */
              child: const Text('OK'),
            ),
          ],
        );
      });
}

void _showRatingAppDialog(BuildContext context) {
  final _ratingDialog = RatingDialog(
    ratingColor: Colors.amber,
    title: 'Rate the Rider',
    message: 'How was the interaction with the rider?',
    //  image: Image.asset("assets/images/devs.jpg",
    //    height: 100,),
    submitButton: 'Submit',
    onCancelled: () {
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      }
      print('cancelled');
      },
    onSubmitted: (response) {
      print('rating: ${response.rating}, '
          'comment: ${response.comment}');

      // call rating API
     // rateTrip(context,response.rating, response.comment);

      if (response.rating < 3.0) {
        print('response.rating: ${response.rating}');
      } else {
        Container();
      }
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      }
    },
  );

  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (context) => _ratingDialog,
  );
}
