import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/widgets/navigationDrawer.dart';
import 'package:flutter/material.dart';

import '/globals.dart' as globals;
class tripsPage extends StatefulWidget {
  static const String routeName = '/tripsPage';
  const tripsPage({Key? key}) : super(key: key);
  @override
  _tripsPageState createState() => _tripsPageState();
}

class _tripsPageState extends State<tripsPage> {
  var tripHistory_;
  int records = 0;


  List<String> titles = [];
  List<String> subtitles = [ ];
  List<String> tripcosts = [];
  List<IconData> icons = [ Icons.verified_rounded , Icons.info, Icons.cancel];

  tripHistory() async {
    String url = globals.BASE_URL;
    String resp = "",
        responseDescription = "",
        responseCode = "",
        availableTrips = "",
        email = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    email = prefs.getString('email') ?? "";

    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'TRIPHIS',
      'user': 'CLIENT',
      'email': email,
      'channel': globals.channel,
    };

    resp = (await globals.apiRequest(url, map));
       jsonObj = json.decode(resp);

    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];
      if (responseCode == "000") // success
        {
      tripHistory_ = jsonObj['tripHistory'] as List;
      setState(() {
        records = tripHistory_.length;
      });
      String delivery = "",
          status = "",
          subT = "",
          pickup = "",amnt="",
          dest = "";

      int index = records - 1,
          j;
      for (int len = 0; len < records; len++) {
        j = index - len;
        delivery = formatDate(tripHistory_[j]['delivery_date']);
        titles.insert(len, "Delivered on : $delivery");
        pickup = tripHistory_[j]['pickup_address'];
        dest = tripHistory_[j]['dropoff_address'];
        amnt = tripHistory_[j]['trip_cost'];
        tripcosts.insert(len, "Cost (ksh.): $amnt /=");
        subT = "From: " + pickup + "\nTo: " + dest;
        subtitles.insert(len, subT);
        status = tripHistory_[j]['status'];
         if (status == 'FULFILLED') {
          icons.insert(len, Icons.verified_rounded);
        } else if (status == 'DISPUTED') {
          //  icons[len] = Icons.warning;
          icons.insert(len, Icons.warning);
        } else {
          // icons[len] = Icons.cancel;
          icons.insert(len, Icons.cancel);
        }
      }
    } else // invalid details
        {

    }
  }

   setSpecTripDets(int index, int index2) {
    globals.tripDetails_tripID = tripHistory_[index]['type_of_trip'];
    globals.tripDetails_date = titles[index2];
    globals.tripDetails_status = tripHistory_[index]['status'];
    globals.tripDetails_pickup = tripHistory_[index]['pickup_address'];
    globals.tripDetails_dropoff = tripHistory_[index]['dropoff_address'];
    globals.tripDetails_client = tripHistory_[index]['receiver_name'];
    globals.tripDetails_cost = tripHistory_[index]['trip_cost'].toString();
    globals.tripDetails_package = tripHistory_[index]['package_description'];
  }

  Widget _buildImage(String assetName, [double width = 350]) {
    return Image.asset('assets/$assetName', width: width);
  }

  @override
  void initState() {
    setState(() {
      tripHistory();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();

    if (records == 0) {
      return Scaffold(
        key:_scaffoldKey,
          appBar: new AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    child:SizedBox(
                      height: 40,
                      width: 40,
                      child: FloatingActionButton(
                        elevation:15,
                        highlightElevation: 10,
                        onPressed: () {
                          _scaffoldKey.currentState!.openDrawer();
                        },
                        child: const Icon(
                          Icons.menu,
                          color:Colors.white,
                        ),
                        backgroundColor: globals.primaryColor,
                        // )
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 00),
                    child: Text(
                      " Previous Trips",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ],
              )),
          drawer: navigationDrawer(),
          body:Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  Divider(
                    thickness: 2,
                    color: globals.accentColor,
                    indent: 20,
                    endIndent: 20,
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.only(),
                    child: Text(
                      'Hi ${globals.name},  You might will be able to view your trip history in afew  ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w300,
                        color: globals.primaryColor,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Divider(
                    thickness: 2,
                    color: globals.accentColor,
                    indent: 20,
                    endIndent: 20,
                  ),
                  SizedBox(height: 100),
                  Container(

                    child:  LoadingBouncingGrid.square(
                      borderColor: Colors.white,
                      borderSize: 3.0,
                      size: 200.0,
                      backgroundColor:globals.accentColor2,
                      duration: Duration(seconds: 120),
                    ),
                  ),               ],
              ),
      ]
      )
      );
    } else {
      return Scaffold(
        key: _scaffoldKey,
       appBar: new AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child:SizedBox(
                  height: 40,
                  width: 40,
                  child: FloatingActionButton(
                    elevation:15,
                    highlightElevation: 10,
                    onPressed: () {
                      _scaffoldKey.currentState!.openDrawer();
                    },
                    child: const Icon(
                      Icons.menu,
                      color:Colors.white,
                    ),
                    backgroundColor: globals.primaryColor,
                    // )
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(20, 0, 0, 00),
                child: Text(
                  " Previous Trips",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: 15,
                      fontWeight: FontWeight.w300),
                ),
              ),
            ],
          )),
      drawer:navigationDrawer(),
        body:  Stack(
            children:[
              Stack(
          children: [
            SizedBox(height:30),

            Container (child:ListView.builder(
                itemCount: records,
                itemBuilder: (context, index) {
                  return Card(
                      child: ListTile(
                          onTap: () {
                            setState(() {
                              setSpecTripDets((records - 1 - index), index);
                            });
                          },
                          title:Column(children: [
                            SizedBox(height:5),
                            Container(
                              alignment: Alignment.topLeft,
                                child:Text(titles[index],style:TextStyle(
                              color:globals.accentColor2, fontWeight: FontWeight.w300, fontSize: 13
                          ))),
                            SizedBox(height:5),]),
                          subtitle: Column(
                              children:[
                                Text(subtitles[index],style:TextStyle(
                            color:globals.primaryColor, fontWeight: FontWeight.w300, fontSize: 12
                          )),
                                SizedBox(height:5),
                                Container(
                                  alignment: Alignment.topLeft,
                                    child:Text(tripcosts[index],textAlign: TextAlign.start,style:TextStyle(
                                    color:globals.accentColor, fontWeight: FontWeight.w300, fontSize: 13
                                ))),
                                SizedBox(height:5),
                              ]),
                          leading: CircleAvatar(
                            radius: 25.0,
                            child: ClipRRect(
                              child: _buildImage('images/slider2.png'),
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                          ),
                          trailing: Icon(icons[index])));
                }))
          ],
        )]),


      );
    }
  }
}

  String formatDate(String dt) {
    String fDate = "";
    var parsedDate = DateTime.parse(dt);
    final DateFormat formatter = DateFormat('dd MMM, yyyy HH:mm');
    fDate = formatter.format(parsedDate);
    return fDate;

}