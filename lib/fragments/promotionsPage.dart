import 'package:loading_animations/loading_animations.dart';

import '/widgets/navigationDrawer.dart';
import 'package:flutter/material.dart';

import '/globals.dart' as globals;

class promotionsPage extends StatelessWidget {
  static const String routeName = '/promotionsPage';

  @override
  Widget build(BuildContext context) {

    final _scaffoldKey = GlobalKey<ScaffoldState>();
    return  WillPopScope(
        onWillPop: () async {
          final shouldPop = await globals.willPopCallback(context) ;
          return shouldPop ?? false;
        },
        child: new Scaffold(
            key: _scaffoldKey,
            appBar: new AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.white,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child:SizedBox(
                        height: 40,
                        width: 40,
                        child: FloatingActionButton(
                          elevation:15,
                          highlightElevation: 10,
                          onPressed: () {
                            _scaffoldKey.currentState!.openDrawer();
                          },
                          child: const Icon(
                            Icons.menu,
                            color:Colors.white,
                          ),
                          backgroundColor: globals.primaryColor,
                          // )
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(20, 0, 0, 00),
                      child: Text(
                        "Currently Available Promotions",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                )),
        drawer: navigationDrawer(),
        body:
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 20),
                Divider(
                  thickness: 2,
                  color: globals.accentColor,
                  indent: 20,
                  endIndent: 20,
                ),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.only(),
                  child: Text(
                    'Hi ${globals.name},  You might also receive promotions via Notifications  ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w300,
                      color: globals.primaryColor,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Divider(
                  thickness: 2,
                  color: globals.accentColor,
                  indent: 20,
                  endIndent: 20,
                ),
                SizedBox(height: 100),
                Container(

                  child:  LoadingBouncingGrid.square(
                    borderColor: Colors.white,
                    borderSize: 3.0,
                    size: 200.0,
                    backgroundColor:globals.primaryColor,
                    duration: Duration(seconds: 120),
                  ),
                ),               ],
            ),

    )
    );
  }
}
