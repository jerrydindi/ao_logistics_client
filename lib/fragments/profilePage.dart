import 'dart:convert';
import 'dart:io' show File, Platform;
import 'package:ao_courier_client/globals.dart' as globals;
import 'package:ao_courier_client/login.dart';
import 'package:ao_courier_client/models/accounts.dart';
import 'package:ao_courier_client/widgets/navigationDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ClientProfileDetails extends StatefulWidget {
  static const String routeName = '/Profile';

  @override
  _ClientProfileDetails createState() => _ClientProfileDetails();
}

class _ClientProfileDetails extends State<ClientProfileDetails> {
  late double _ratings = 5;
  late double _accountNumber = 0.0;
  late double _accountBalance = 0.0;
  late String _clientType = "";
  late String _clientName = "";
  late String _clientEmail = "";
  late String _corprateCode = "";
  late String email = "";
  late var splitEmail = [];
  late String _accountType = "";
  late Accounts clientAccount;

  final ImagePicker imagePicker = ImagePicker();
  late File profileImage;
  late var image = AssetImage("assets/images/profile.png");

  loginPersistNot() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('loginPersist', "0");
  }

  otpPersistNot() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('otpPersist', "0");
  }

  makeAPICall(BuildContext context) async {
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'INDIVIDUAL_ACCOUNT',
      'email': globals.email,
      'channel': globals.channel,
    };
    resp = (await globals.apiRequest(url, map));
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];
    var accounts = jsonObj['accounts'];
    print("== > RESP $accounts");

    if (responseCode == "000") // change password
    {
      setState(() {
        _accountNumber = double.parse(accounts[0]['accountNumber'].toString());
        _accountBalance = jsonObj['accounts'][0]['balance'];
        _clientType = jsonObj['accounts'][0]['accountName'];
        _clientEmail = jsonObj['accounts'][0]['email'];
        _clientName = jsonObj['accounts'][0]['accountName'];
        splitEmail = _clientEmail.split("@");

        _accountType = jsonObj['accounts'][0]['accountType'];
      });
      if (jsonObj['accounts'][0]['corporateCode'] == null ||
          jsonObj['accounts'][0]['corporateCode'] == "") {
        setState(() {
          _corprateCode = "No Corprate Account";
        });
      } else {
        setState(() {
          _corprateCode = jsonObj['accounts'][0]['corporateCode'];
        });
      }
      String topic_specific =
          'ao_courier_rider_' + email.substring(0, email.indexOf("@"));
    } else // invalid details
    {}
  }

  _profileCircleTest(image) {
    return Card(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100.0),
        ),
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(15),
          child: Image.asset(
            "assets/images/profile.png",
            fit: BoxFit.scaleDown,
            alignment: Alignment.bottomRight,
            height: 110,
          ),
        ));
  }

  _profileDetailsSection(
      String clientName,
      double clientRating,
      String clientEmail,
      String accountNumber,
      String accountBalance,
      String corprateID,
      String accountType) {
    return Column(
      children: [
        SizedBox(height: 10),
        /* name n email*/
        Text(
          clientName,
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.w800),
        ),
        SizedBox(height: 5),
        Text(
          clientEmail,
          style: TextStyle(fontSize: 13, color: Colors.grey[500]),
        ),
        SizedBox(height: 25),
        /* ACCOUNT NUMBER*/
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Text(
                "My Account Number : ",
                style: TextStyle(
                    fontWeight: FontWeight.w700, color: Colors.grey[900]),
              ),
            ),
            Expanded(
              // padding: EdgeInsets.fromLTRB(10, 0, 20, 0),
              child: Text(accountNumber),
            ),
          ],
        ),
        SizedBox(height: 10),
        Divider(
          indent: 20,
          endIndent: 20,
          color: Colors.grey,
        ),
        SizedBox(height: 10),
        /* ACCOUT balance */
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Text(
                "My Account Balance : ",
                style: TextStyle(
                    fontWeight: FontWeight.w700, color: Colors.grey[900]),
              ),
            ),
            Expanded(
                // padding: EdgeInsets.fromLTRB(10, 0, 30, 0),
                child: Text(accountBalance)),
          ],
        ),
        SizedBox(height: 10),
        Divider(
          indent: 20,
          endIndent: 20,
          color: Colors.grey,
        ),
        SizedBox(height: 10),
        /* Corprate ID */
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Text(
                "My Corprate ID : ",
                style: TextStyle(
                    fontWeight: FontWeight.w700, color: Colors.grey[900]),
              ),
            ),
            Expanded(
                // padding: EdgeInsets.fromLTRB(10, 0, 20, 0),
                child: (corprateID == "")
                    ? Text("No Corprate Account")
                    : Text(corprateID)),
          ],
        ),
        SizedBox(height: 10),
        Divider(
          indent: 20,
          endIndent: 20,
          color: Colors.grey,
        ),
        SizedBox(height: 10),
        /* Account Type ID */
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              // alignment: Alignment.center,
              child: Text(
                "My Account Type : ",
                style: TextStyle(
                    fontWeight: FontWeight.w700, color: Colors.grey[900]),
              ),
            ),
            Expanded(
                //  padding: EdgeInsets.fromLTRB(10, 0, 20, 0),
                child: Text(
              accountType,
              textAlign: TextAlign.justify,
            )),
          ],
        ),
        SizedBox(height: 10),
        Divider(
          indent: 20,
          endIndent: 20,
          color: Colors.grey,
        ),
        SizedBox(height: 10),

        /* My Rating */
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              // padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Text(
                "My Current Rating : ",
                style: TextStyle(
                    fontWeight: FontWeight.w700, color: Colors.grey[900]),
              ),
            ),
            Expanded(
                child: RatingBarIndicator(
              rating: clientRating,
              itemBuilder: (context, index) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              itemCount: 5,
              itemSize: 25.0,
              direction: Axis.horizontal,
            )),
          ],
        ),
        SizedBox(height: 6),
        Divider(
          indent: 20,
          endIndent: 20,
          color: Colors.grey,
        ),
        SizedBox(height: 25),
      ],
    );
  }

  _selectYourImage() {
    return Container(
        height: 100,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          children: <Widget>[
            Text("Choose your profile photo ."),
            SizedBox(height: 20),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextButton.icon(
                    label: Text(
                      "Camera",
                      style: TextStyle(color: Colors.grey),
                    ),
                    icon: Icon(Icons.camera),
                    onPressed: () {
                      _takePhoto(ImageSource.camera);
                    },
                  ),
                ),
                Expanded(
                    child: TextButton.icon(
                  label: Text(
                    "Gallery",
                    style: TextStyle(color: Colors.grey),
                  ),
                  icon: Icon(Icons.image),
                  onPressed: () {
                    _takePhoto(ImageSource.gallery);
                  },
                ))
              ],
            )
          ],
        ));
  }

  _takePhoto(ImageSource imageSource) async {
// final ImagePicker imagePicker = ImagePicker();
    final pickedFile = await imagePicker.pickImage(
      source: imageSource,
    );
    setState(() {
      profileImage = (pickedFile!.path) as File;
    });
  }

  void _networkCalls() {
    //  TODO: Implementation of Server client details and rating fetch procedure .
  }

  @override
  void initState() {
    makeAPICall(context);
    if (email == "") {
      setState(() {
        email = "useremail@gmail.com";
        splitEmail = email.split("@");
      });
    } else {
      setState(() {
        splitEmail = email.split("@");
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    late final icon;
    if (Platform.isAndroid) {
      icon = Icons.brightness_6;
    } else {
      icon = CupertinoIcons.moon_stars;
    }
    // if (Platform.isAndroid) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.white,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  child: SizedBox(
                    height: 40,
                    width: 40,
                    child: FloatingActionButton(
                      elevation: 15,
                      highlightElevation: 10,
                      onPressed: () {
                        _scaffoldKey.currentState!.openDrawer();
                      },
                      child: const Icon(
                        Icons.menu,
                        color: Colors.white,
                      ),
                      backgroundColor: globals.primaryColor,
                      // )
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 00),
                  child: Text(
                    "Your Profile",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            )),
        drawer: navigationDrawer(),
        body: ListView(
          children: [
            Column(children: <Widget>[
              SizedBox(height: 30),
              Stack(
                children: [
                  _profileCircleTest(image),
                ],
              ),
              /* Profile details Card */
              new Container(
                margin: EdgeInsets.only(left: 30, right: 30),
                alignment: Alignment.bottomCenter,
                child: _profileDetailsSection(
                    //TODO : ADD THE LINK TO THE BACKEND FOR RATING SECTION
                    _clientName,
                    _ratings,
                    _clientEmail,
                    _accountNumber.toString(),
                    " ksh. ${_accountBalance.toString()}",
                    _corprateCode,
                    _accountType),
              ),
              SizedBox(height: 5),
              InkWell(
                  onTap: () {
                    print("== > signing out ..");
                    // reset login flags
                    loginPersistNot();
                    otpPersistNot();
                    Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (_) => LoginPage()),
                      (Route<dynamic> route) => false,
                    );
                  },
                  child: Card(
                      margin: EdgeInsets.only(left: 30, right: 30),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 5,
                      child: Container(
                        //width: 200,
                        //  padding: EdgeInsets.only(left: 55, right: 55),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.fromLTRB(13, 10, 10, 10),
                                child: Icon(Icons.logout_outlined,
                                    color: globals.accentColor, size: 20)),
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 10, 10),
                              child: Text(
                                "Sign Out",
                                style: TextStyle(
                                    color: globals.accentColor,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            SizedBox(height: 10)
                          ],
                        ),
                      )))
            ]
            )
          ],
        ));
  }
}
