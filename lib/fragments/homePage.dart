import 'package:flutter/services.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import '../completeTripRequest.dart';
import '../globals.dart' as globals;
import '../map.dart';
import '../packageDetails.dart';
import '/widgets/navigationDrawer.dart';
import 'package:flutter/material.dart';

class homePage extends StatefulWidget {
  static const String routeName = '/homePage';

  @override
  _homePage createState() => _homePage();
}

class _homePage extends State<homePage> {
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  // bool shouldPop = true;
  _animation() {
    return SmoothPageIndicator(
      controller: globals.pageController,
      count: 3,
      effect: WormEffect(dotHeight: 10,dotWidth: 10),
      onDotClicked: (index) {
        globals.pageController.animateToPage(index,
            duration: Duration(milliseconds: 500), curve: Curves.easeInCubic);
      },
    );
  }

  _pageindicator() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      // padding: EdgeInsets.all(10),
      children: [
        Center(
          child: _animation(),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  _pageView(BuildContext context) {
    return Stack(children: [
      //SizedBox(height: 100),

      PageView(
        controller: globals.pageController,
        children: [MapView(), PackageDetails(), MapViewTrack()],
      ),
      _pageindicator(),
    ]);
  }

  _pageUI() {
    return new Scaffold(
        key: _scaffoldkey,
        drawer: navigationDrawer(),
        body: _pageView(context));
  }

  @override
  Widget build(BuildContext context) {
    //  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    return WillPopScope(
      child: _pageUI(),
      onWillPop: () async {
        // Navigator.of(context).pop(false);
        // (Route<dynamic> route) => false;
        return false;
      },
        // onWillPop: () async {
        //   return false;
        // });

    );
  }
}
