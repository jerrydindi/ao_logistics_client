import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'globals.dart' as globals;
import 'login.dart';
import 'otp.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  makeAPICall(String email, String name, String phoneNumber,  BuildContext context) async {
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'ADDCLNT',
      "fullname" :name,
      "phonenumber" : phoneNumber,
      'email': email,
      'channel': globals.channel,
    };
print("MAp == > $map");
    resp = (await globals.apiRequest(url, map));
    print(resp);
    jsonObj = json.decode(resp);

    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];

    // Navigator.pop(context);
    Fluttertoast.showToast(
        msg: responseDescription,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
    if (responseCode == "011") // change password
    {
      prefs.setString('email', email);
      prefs.setString('phoneNumber', phoneNumber);

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
            builder: (_) => OTPPage()),
      );
    } else if (responseCode == "000") // success
    {
      prefs.setString('email', email);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
            builder: (_) => OTPPage()),
      );
      String topic_specific =
          'ao_courier_rider_' + email.substring(0, email.indexOf("@"));
      print(topic_specific);
      await FirebaseMessaging.instance.subscribeToTopic(topic_specific);

    } else // invalid details
    {}
  }

  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phonenumberController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
            /* appBar: AppBar(
          title: Text('Sample App'),
        ), */
            body: Padding(
                padding: EdgeInsets.all(0),
                child: ListView(
                  children: <Widget>[
                    Card(
                      color: globals.primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Column(
                        children: [
                          SizedBox(height:10),
                          Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              child: Text(
                                'Sign Up',
                                style: TextStyle(
                                    fontSize: 20, color: Colors.white , fontWeight: FontWeight.w300),
                              )),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                              style: TextStyle(color: Colors.white70),
                              controller: nameController,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide:
                                    new BorderSide(color: Colors.white70)),
                                labelStyle: TextStyle(color: Colors.white70),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: new BorderSide(
                                        color: Colors.white)),
                                labelText: 'Full Name',
                                prefixIcon: const Icon(
                                  Icons.account_circle_rounded,
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                                keyboardType:TextInputType.number,
                              style: TextStyle(color: Colors.white70),
                              controller: phonenumberController,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide:
                                    new BorderSide(color: Colors.white70)),
                                labelStyle: TextStyle(color: Colors.white70),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: new BorderSide(
                                        color: Colors.white)),
                                labelText: 'Phone Number',
                                prefixIcon: const Icon(
                                  Icons.phone,
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                              style: TextStyle(color: Colors.white70),
                              controller: emailController,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide:
                                    new BorderSide(color: Colors.white70)),
                                labelStyle: TextStyle(color: Colors.white70),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: new BorderSide(
                                        color: Colors.white)),
                                labelText: 'Email',
                                prefixIcon: const Icon(
                                  Icons.email_outlined,
                                  color: Colors.white70,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                            enableFeedback: true,
                            onTap: () {},
                            child: Container(
                                height: 70,
                                width: 200,
                                padding: EdgeInsets.all(10),
                                child: Card(
                                    color: globals.primaryColor,
                                    elevation: 5,
                                    shape: RoundedRectangleBorder(
                                      side:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: TextButton(
                                      child: Text(
                                        'Sign-Up',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      onPressed: () {
                                          makeAPICall(emailController.text.trim(),nameController.text.trim(),phonenumberController.text.trim(), context);


                                      },
                                    ))),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(builder: (_) => LoginPage()),
                              );
                            },
                            child: Text(
                              'Have an account? Sign in',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                decoration:TextDecoration.underline,
                                color: Colors.grey[200],
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ),

                   
                        ],
                      ),
                    ),
                    Container(
                      padding:EdgeInsets.all(20),
                      child: Positioned.fill(
                        child: Image.asset(
                            "assets/images/login.png",
                            fit: BoxFit.none,
                            alignment: Alignment.bottomRight,
                            height: 200,
                            scale: 1.4),

                      ),
                    ),

                  ],
                ))));
  }
}
