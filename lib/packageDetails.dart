import 'dart:convert';

//import 'package:ao_courier_client/widgets/navigationDrawer.dart';
import 'package:ao_courier_client/map.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'fragments/homePage.dart';
import 'globals.dart' as globals;

class PackageDetails extends StatefulWidget {
  static const String routeName = '/PackageDetails';

  @override
  _PackageDetailsState createState() => _PackageDetailsState();
}

class _PackageDetailsState extends State<PackageDetails>
    with AutomaticKeepAliveClientMixin<PackageDetails> {
  bool _client = false;
  bool _receiver = false;
  bool _corprate = false;
  bool _express = true;
  bool _standard = false;
  double opacityLevel = 0.0;
  final startAddressController = TextEditingController();
  final recipientNameController = TextEditingController();
  final recipientPhoneController = TextEditingController();
  final packageDescriptionController = TextEditingController();
  final startAddressFocusNode = FocusNode();
  final recipientNameFocusNode = FocusNode();
  final recipientPhoneFocusNode = FocusNode();

  final DateFormat formater = DateFormat('dd MMM yyyy');

  //latLng
  late String pickLat = "";
  late String pickLng = "";
  late String dropLat = "";
  late String dropLng = "";
  late String pickUpAddress = "";
  late String dropOffAddress = "";
  late String tripType = "EXPRESS";
  late String billingAccnt = "INDIVIDUAL";
  late String tripCost = "";
  late String packageDetails = "";
  late String scheduledTime = "";
  late String dateToUI = formater.format(DateTime.now());
  late String dateToServer = "";
  late String dateCard = "Delivery Date";
  late String clientEmail = "";
  late String servicePayer = "";
  late String servicePayerID = "";
  late bool _hasCoporateAccount = false;
  late String corporateID = "";
  late String corporateEmail = "";
  late String corporateName = "";

// used to store the split'd the email address into sections
  late var splitEmail = [];

  fetchCorporateDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    corporateID = prefs.getString('corporateID') ?? "";
    corporateEmail = prefs.getString('corporateEmail') ?? "";
    corporateName = prefs.getString('corporateName') ?? "";

    _displayCorporateCard();
  }

  _displayCorporateCard() {
    setState(() {
      if (corporateName == "") {
        _hasCoporateAccount = false;
      } else {
        _hasCoporateAccount = true;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    selectedDate = DateTime.now();
    fetchCorporateDetails();
  }

  void _changeOpacity() {
    setState(() => opacityLevel = opacityLevel == 0 ? 1.0 : 0.0);
  }

  void _changeOpacityToZero() {
    setState(() => opacityLevel = opacityLevel == 0 ? 1.0 : 0.0);
  }

  String date = "";
  DateTime selectedDate = DateTime.now();
  DateTime lastdate = DateTime.now().add(Duration(days: 30));
  TimeOfDay selectedTime = TimeOfDay(hour: DateTime.now().hour, minute: DateTime.now().minute + 10);
  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      //* To be Editied From the current yeear
      initialDate: selectedDate,
      firstDate: selectedDate,
      lastDate: lastdate,
      useRootNavigator: true
    );
    if (selected == null || _express == true || _standard != true) {
      setState(() {
        final DateFormat formatter = DateFormat('dd-MM-yyyy HH:mm:ss');
        final String dropDate = formatter.format(selectedDate);
        dateToServer = dropDate;
        final DateFormat formater = DateFormat('dd MMM yyyy');
        final String dropPackageDate = formater.format(selectedDate);
        dateToUI = dropPackageDate;
      });
    }

    // display time
    if (selected != null)
      {
        final TimeOfDay? picked = await showTimePicker(
          context: context,
          initialTime: selectedTime,
        );
        if (picked != null)
          setState(() {
            selectedTime = picked;
            // _hour = selectedTime.hour.toString();
            // _minute = selectedTime.minute.toString();
            // _time = _hour + ' : ' + _minute;
            // _timeController.text = _time;
            // _timeController.text = formatDate(
            //     DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            //     [hh, ':', nn, " ", am]).toString();
          });
      }
    //if (selected != null && selected != selectedDate) {
    if (selected != null && _express == false && _standard == true) {
      setState(() {
        final DateFormat formater = DateFormat('dd MMM yyyy');
        final String dropPackageDate = formater.format(selected);
        String hour = "", min = "", sec = "00";
        if (selectedTime.hour >= 0 && selectedTime.hour <= 9)
          {
            hour = "0" + selectedTime.hour.toString();
          }
        else
          {
            hour = selectedTime.hour.toString();
          }

        if (selectedTime.minute >= 0 && selectedTime.minute <= 9)
        {
          min = "0" + selectedTime.minute.toString();
        }
        else
        {
          min = selectedTime.minute.toString();
        }

        dateToUI = dropPackageDate + " " + hour + ":" + min;
        //to server
        //final DateFormat formatter = DateFormat('dd-MM-yyyy HH:mm:ss');
        final DateFormat formatter = DateFormat('dd-MM-yyyy');
        final String dropDate = formatter.format(selected);
        dateToServer = dropDate + " " + hour + ":" + min + ":" + sec;
        scheduledTime = dateToServer;
      });
    }
    if (selected == null) {
      setState(() {
        selectedDate = selected!;
      });
    }
    // print("Time Selected  == > $dateToServer");
  }

  /** MAKE TRIP REQUEST */
  makeAPICall(BuildContext context, String servicePayerID) async {
    // print("making trip req .....");
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    // print(" === > Shared PRefs .....");
    SharedPreferences _sharedPreferences =
    await SharedPreferences.getInstance();
    _sharedPreferences.setString("servicePayerID", servicePayerID);

    if (_sharedPreferences.getString("pickUpAddress") == null ||
        _sharedPreferences.getString("dropOffAddress") == null) {
      dropOffAddress = "Kindly Select Location ";
      pickUpAddress = "Kindly Select Location ";
    } else {
      setState(() {
        dropOffAddress = _sharedPreferences.getString("pickUpAddress")!;
        pickUpAddress = _sharedPreferences.getString("dropOffAddress")!;
      });
    }
    if (_corprate == true)
      clientEmail = corporateEmail;
    else
      clientEmail = globals.email;

    // print(" === > Shared PRefs client.....$clientEmail");

    // if (_sharedPreferences.getString('pickUpLat') == null ||
    //     _sharedPreferences.getString('pickUpLng') == null ||
    //     _sharedPreferences.getString('dropOffLat') == null ||
    //     _sharedPreferences.getString('dropOffLng') == null ||
    //     _sharedPreferences.getString("billingAccount") == null ||
    //     _sharedPreferences.getString("tripCost") == null) {
    //   pickLat = "";
    //   pickLng = "";
    //   dropLat = "";
    //   dropLng = "";
    //   billingAccnt = "INDIVIDUAL";
    //   tripCost = "200 ";
    //   globals.newTrip_pickup_lat = pickLat; //pickLat
    //   globals.newTrip_pickup_long = pickLng; // =pickLng;
    //   globals.newTrip_dropoff_lat = dropLat; //= dropLat;
    //   globals.newTrip_dropoff_long = dropLng; //= dropLng ;
    // } else {
    //   pickLat = _sharedPreferences.getString('pickUpLat')!;
    //   pickLng = _sharedPreferences.getString('pickUpLng')!;
    //   dropLat = _sharedPreferences.getString('dropOffLat')!;
    //   dropLng = _sharedPreferences.getString('dropOffLng')!;
    //   billingAccnt = _sharedPreferences.getString("billingAccount")!;
    //   tripCost = _sharedPreferences.getString("tripCost")!;
    // }

    pickLat = _sharedPreferences.getString('pickUpLat') ?? "";
    pickLng = _sharedPreferences.getString('pickUpLng') ?? "";
    dropLat = _sharedPreferences.getString('dropOffLat') ?? "";
    dropLng = _sharedPreferences.getString('dropOffLng') ?? "";
    billingAccnt = _sharedPreferences.getString("billingAccount") ?? "INDIVIDUAL";
    tripCost = _sharedPreferences.getString("tripCost") ?? "200";

    // print(" ===> Details $pickLat ||  $pickLng   || $dropLat ||  $dropLng ");
    globals.newTrip_delivery_type = tripType;
    globals.newTrip_package = packageDetails;
    _sharedPreferences.setString('packageDetails', packageDetails);
    _sharedPreferences.setString('payerDetails', servicePayer);
    _sharedPreferences.setString('billingAccnt', billingAccnt);

    _sharedPreferences.setString('pickLat', pickLat);
    _sharedPreferences.setString('pickLng', pickLng);
    _sharedPreferences.setString('dropLat', dropLat);
    _sharedPreferences.setString('dropLng', dropLng);

    globals.newTrip_date = scheduledTime;
    // print("making trip request .....");

    // print(
    //     "locations == > $pickLat === > $pickLng ==> $dropLat ==> $dropLng == >$packageDetails == > $scheduledTime  ==> $billingAccnt.....");

     globals.newTrip_pickup_lat = pickLat;
     globals.newTrip_pickup_long =pickLng;
     globals.newTrip_dropoff_lat = dropLat;
     globals.newTrip_dropoff_long = dropLng;
    // print(
    //     " === > PICK ${globals.newTrip_pickup}  ==> DROP ${globals.newTrip_dropoff}");
    // print(" === > PICKcoordz  $pickLng,  ==> DROP $dropLat , $dropLng ");
    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'REQTRIP',
      "pickup_latitude": globals.newTrip_pickup_lat,
      "pickup_longitude": globals.newTrip_pickup_long,
      "drop_off_latitude": globals.newTrip_dropoff_lat,
      "drop_off_longitude": globals.newTrip_dropoff_long,
      "pickup_name": globals.newTrip_pickup,
      "drop_off_name": globals.newTrip_dropoff,
      "package_type": "SMALL",
      "trip_type": globals.newTrip_delivery_type,
      "client_email": clientEmail,
      "package_description": globals.newTrip_package,
      "delivery_date": globals.newTrip_date,
      "receiver_name": globals.newTrip_recipient_name,
      "receiver_phone": globals.newTrip_recipient_phone,
      "billing_account": billingAccnt,
      "payment_by": servicePayerID,
      "corporate_id": corporateID,
      "promotion_code": globals.promoCode,
      'channel': globals.channel,
    };
     print("MAP === > $map");
    resp = (await globals.apiRequest(url, map));
    // print(resp);
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];
    // print("RESPONSE CODE === > $responseCode \n RESPONSE === > $jsonObj");
// print("==> reqres $resp");
//  Navigator.pop(context);
    Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      // fontSize: 16.0
    );
    //  Toast.show(responseDescription, context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    if (responseCode == "000") // success
    {
      if (_standard == true)
        {
          displayAlertDialog("Standard Delivery","Delivery successfully scheduled for " + dateToUI + ". You will be notified via SMS when rider is on the way.");
        }
      else {
        globals.newTrip_cost = jsonObj['tripCost'].toString();
        globals.newTrip_tripID = jsonObj['tripCode'];
        globals.newTrip_distance = jsonObj['tripDistance'];
        globals.newTrip_time_estimate = jsonObj['tripDuration'];
        globals.packageDetailsOK = true;
        globals.requestedTrip = true;
        _sharedPreferences.setString('tripCode', jsonObj['tripCode']);
        _sharedPreferences.setString('delivery_date', dateToServer);
        _sharedPreferences.setString(
            'tripCost', jsonObj['tripCost'].toString());
        _sharedPreferences.setString('packageDetails', packageDetails);
        _sharedPreferences.setString('pickupAddress', globals.newTrip_pickup);
        _sharedPreferences.setString(
            'destinationAddress', globals.newTrip_dropoff);

        // == >  MOVE NEXT PAGE
        globals.pageController.animateToPage(2,
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInCubic);
      }
    }
    if(responseCode == "111") {
      displayAlertDialog("Riders Unavailable","Kindly schedule delivery at a later time using the STANDARD option.");
      // Fluttertoast.showToast(
      //   msg: " Kindly schedule this trip as a STANDARD or wait for 5min then request once more .. ",
      //   toastLength: Toast.LENGTH_LONG,
      //   gravity: ToastGravity.BOTTOM,
      //   timeInSecForIosWeb: 1,
      // );
      setState(() {
        globals.requestedTrip == false;
      });
    }else
    // booking trip unsuccessful
    {}
  }

  _allPackageDetails() {
    return ListView(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(20, 0, 25, 0),
          child: _serviceType(),
        ),
        Container(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 00),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: globals.primaryColor,
                      elevation: 5,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.only(
                              topRight: Radius.circular(0.0),
                              topLeft: Radius.circular(10.0),
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(0.0))),
                    ),
                    onPressed: () {
                      if (_standard)
                        {
                          _selectDate(context);
                        }
                    },
                    child: Text(
                      dateCard,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                Container(
                    // padding: EdgeInsets.fromLTRB(10, 0, 10, 00),
                    child: Card(
                        color: globals.accentColor2,
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10.0),
                              topLeft: Radius.circular(0.0),
                              bottomLeft: Radius.circular(0.0),
                              bottomRight: Radius.circular(10.0)),
                        ),
                        child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Text(
                              "$dateToUI",
                              style: TextStyle(color: Colors.white),
                            )))),
              ]),
        ),

        Container(
            child: Card(
          color: Colors.grey.shade50,
          margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(15.0),
                topLeft: Radius.circular(15.0),
                bottomLeft: Radius.circular(15.0),
                bottomRight: Radius.circular(15.0)),
          ),
          child: Column(
            children: <Widget>[
              // Container(child: _inputFields()),
              Container(child: _inputFields()),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 15),
                  child: _payerOfService())
            ],
          ),
        )),
        SizedBox(height: 5),
        /** ==> SUbmit request  */
        Align(
          alignment: Alignment(0.0, 0.0),
          child: Visibility(
            visible: (_receiver || _client || _corprate) && (_express || _standard)
                ? true
                : false,
            child: ElevatedButton(
              onPressed: (recipientNameController.text != '' &&
                      recipientPhoneController.text != '')
                  ? () async {
                      // == >  TRIPDATE
                      globals.tripDetails_date =
                          "${selectedDate.day}/${selectedDate.month}/${selectedDate.year}";

                      // == >  PACKAGE DETAILS
                      globals.packageDetailsOK = true;
                     globals.newTrip_recipient_name =
                          recipientNameController.text;
                      globals.newTrip_recipient_phone =
                          recipientPhoneController.text;
                      makeAPICall(context, servicePayerID);
                    }
                  : null,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Request Delivery'.toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.0,
                  ),
                ),
              ),
              style: ElevatedButton.styleFrom(
                primary: globals.accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.grey.shade200,
          appBar: new AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.white,
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                 Container(
                   child:SizedBox(
                     height: 40,
                     width: 40,
                     child: FloatingActionButton(
                       elevation: 15,
                       highlightElevation: 15,
                       onPressed: () {
                         globals.pageController.animateToPage(0,
                             duration: Duration(milliseconds: 500),
                             curve: Curves.easeInCubic);
                       },
                       child: const Icon(Icons.arrow_back,
                           color: Colors.white, size: 15),
                       backgroundColor: globals.primaryColor,
                       foregroundColor: globals.accentColor2,
                     ),
                   ),
                 ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 00),
                    child: Text(
                      " Select Your Preferred Service ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ],
              )),
          body: _allPackageDetails(),
        ),
        onWillPop: () async {
          return false;
        });
  }

// === >  textFields < ===

  Widget _textField({
    required TextEditingController controller,
    required FocusNode focusNode,
    required String label,
    required String hint,
    required double width,
    required Icon prefixIcon,
    Widget? suffixIcon,
    required Function(String) locationCallback,
  }) {
    return Container(
      width: width * 0.75,
      child: TextField(
        onChanged: (value) {
          locationCallback(value);
        },
        style: TextStyle(
          fontSize: 12,
          color: Colors.grey.shade700,
          fontWeight: FontWeight.w600,
        ),
        controller: controller,
        focusNode: focusNode,
        decoration: new InputDecoration(
          prefixIcon: prefixIcon,
          // suffixIcon: suffixIcon,
          labelText: label,
          labelStyle: TextStyle(
              fontWeight: FontWeight.w300,
              color: focusNode.hasFocus
                  ? globals.primaryColor
                  : globals.accentColor2),
          filled: true,
          fillColor: Colors.white,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            borderSide: BorderSide(
              color: Colors.grey.shade300,
              width: 1,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            borderSide: BorderSide(
              color: globals.primaryColor,
              width: 1,
            ),
          ),
          contentPadding: EdgeInsets.all(15),
          hintText: hint,
        ),
      ),
    );
  }

// === >  standard n express cards < ===
  _serviceType() {
    final String expressRide = 'assets/images/express.svg';
    final String standardRide = 'assets/images/standard.svg';
    // Color _selectedCard = Colors.blue.shade200;
    Color _selectedCard = globals.primaryColor;
    Color _notSelectedCard = Colors.white;
    // Color _selectedCardFont = Colors.grey.shade900;
    Color _selectedCardFont = Colors.white;
    Color _nonSelectedCardFont = Colors.grey.shade800;
    Color _selectedCardBikeShadow = Colors.white;
    Color _nonSelectedCardBikeShadow = Colors.blue.shade200.withOpacity(0.5);
    return (Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        AnimatedContainer(
          duration: const Duration(seconds: 10),
          curve: Curves.easeIn,
          child: Card(
            margin: EdgeInsets.fromLTRB(25, 10, 10, 10),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15.0),
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0),
                  bottomRight: Radius.circular(15.0)),
            ),
            color: (_standard == false && _express == true)
                ? _selectedCard
                : _notSelectedCard,
            elevation: 5,
            child: InkWell(
                highlightColor: Colors.blue.withOpacity(0.5),
                customBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15.0),
                      topLeft: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0)),
                ),
                onTap: () {
                  _changeOpacityToZero();
                  //TODO: ADD THIS FOR PURPOSE SOF API REQUEST :  globals.tripDetails_delivery_type = "EXPRESS";
                  setState(() {
                    _standard = false;
                    _express = true;
                    tripType = "EXPRESS";
                    selectedDate = DateTime.now();
                    final DateFormat formatter =
                        DateFormat('dd-MM-yyyy HH:mm:ss');
                    final String currentDate = formatter.format(selectedDate);
                    dateToServer = currentDate;
                    final DateFormat formater = DateFormat('dd MMM yyyy');
                    final String dropPackageDate =
                        formater.format(selectedDate);
                    dateToUI = dropPackageDate;

                    scheduledTime = currentDate;
                    dateCard = "Delivery Date ";
                  });
                },
                child: Column(
                  children: <Widget>[
                    Container(
                      // margin: EdgeInsets.fromLTRB(5,0,5,0),
                      child: Text(""),
                    ),
                    Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(45.0),
                              topLeft: Radius.circular(45.0),
                              bottomLeft: Radius.circular(45.0),
                              bottomRight: Radius.circular(0.0)),
                          // color: const Color(0xffdaf4f8)
                          color: (_standard == false && _express == true)
                              ? _selectedCardBikeShadow
                              : _nonSelectedCardBikeShadow,
                        ),

                        // color: Colors.lightBlueAccent,
                        width: 90,
                        height: 70,
                        padding: EdgeInsets.fromLTRB(3, 20, 3, 10),
                        child: SvgPicture.asset(
                          expressRide,
                        )),
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 20),
                      // padding: EdgeInsets.all(20),
                      child: Text(
                        " Express ",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: (_standard == false && _express == true)
                                ? _selectedCardFont
                                : _nonSelectedCardFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )),
          ),
        ),

        Container(child:AnimatedContainer(
          duration: const Duration(seconds: 10),
          curve: Curves.easeIn,
          child: Card(
            // margin: EdgeInsets.all(10),
            color: (_standard == true && _express == false)
                ? _selectedCard
                : _notSelectedCard,
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15.0),
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0),
                  bottomRight: Radius.circular(15.0)),
            ),
            child: InkWell(
              // splashColor:Colors.blue[200],
                highlightColor: Colors.blue.withOpacity(0.5),
                customBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15.0),
                      topLeft: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0)),
                ),
                onTap: () {
                  _changeOpacity();
                  //TODO: ADD THIS FOR PURPOSE SOF API REQUEST :  globals.tripDetails_delivery_type = "STANDARD";
                  setState(() {
                    _standard = true;
                    _express = false;
                    tripType = "STANDARD";
                    dateCard = "Choose Date";

                  });
                },
                child: Column(
                  children: <Widget>[
                    Container(
                      // margin: EdgeInsets.fromLTRB(5,0,5,0),
                      child: Text(""),
                    ),
                    Container(
                      // margin: EdgeInsets.fromLTRB(5,5,5,0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(45.0),
                              topLeft: Radius.circular(45.0),
                              bottomLeft: Radius.circular(45.0),
                              bottomRight: Radius.circular(0.0)),
                          // color: const Color(0xffdaf4f8)
                          color: (_standard == true && _express == false)
                              ? _selectedCardBikeShadow
                              : _nonSelectedCardBikeShadow,
                        ),
                        width: 90,
                        height: 70,
                        padding: EdgeInsets.fromLTRB(5, 20, 5, 10),
                        child: SvgPicture.asset(
                          standardRide,
                          // semanticsLabel: 'Acme Logo'
                        )),
                    Container(
                      padding: EdgeInsets.fromLTRB(30, 10, 30, 20),
                      child: Text(
                        "Standard",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: (_standard == true && _express == false)
                                ? _selectedCardFont
                                : _nonSelectedCardFont,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                )),
          ),
        ),)
      ],
    ));
  }

// === >  cash n mpesa cards ! this is to be changed to payer  < ===
  _payerOfService() {
    Color _green = Colors.green.withOpacity(0.5);
    Color _cardColor = Colors.white;
    Color _amberOutter = Colors.amber.shade400;
    Color _amberInner = Colors.amber.shade200;
    return (SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AnimatedContainer(
              duration: const Duration(seconds: 10),
              curve: Curves.easeIn,
              width: 150,
              child: Card(
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                color: (_receiver == false &&
                        _client == true &&
                        _corprate == false)
                    ? _amberOutter
                    : _cardColor,
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15.0),
                      topLeft: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0)),
                ),
                child: InkWell(
                    // splashColor:Colors.blue[200],
                    highlightColor: Colors.blue.withOpacity(0.5),
                    customBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15.0),
                          topLeft: Radius.circular(15.0),
                          bottomLeft: Radius.circular(15.0),
                          bottomRight: Radius.circular(15.0)),
                    ),
                    onTap: () {
                      setState(() {
                        _receiver = false;
                        _client = true;
                        _corprate = false;
                        _cardColor = _green;
                        servicePayerID = "1";
                        if (clientEmail == "") {
                          clientEmail = "useremail@gmail.com";
                          splitEmail = clientEmail.split("@");
                          servicePayer = splitEmail[0];
                          globals.whomwillPay = servicePayer;
                        }

                        globals.whomwillPay = servicePayer;
                        // print("Person To Pay  ==== > $servicePayer");
                      });
                    },
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 5),
                            Container(
                              // margin: EdgeInsets.fromLTRB(5,5,5,0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(45.0),
                                      topLeft: Radius.circular(45.0),
                                      bottomLeft: Radius.circular(45.0),
                                      bottomRight: Radius.circular(0.0)),
                                  // color: const Color(0xffdaf4f8)
                                  color: _amberInner),
                              // width: 90,
                              // height: 70,
                              padding: EdgeInsets.all(5),
                              child: Icon(Icons.account_circle_rounded,
                                  size: 15, color: Colors.white),
                            ),
                            Container(
                              padding: EdgeInsets.all(5),
                              child: FittedBox(
                                fit: BoxFit.fitWidth,
                                child: Text(
                                  "I Will Pay for\n the service",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: (_receiver == false &&
                                              _client == true)
                                          ? _cardColor
                                          : Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                            SizedBox(width: 5),
                          ],
                        ),
                        SizedBox(height: 5),
                      ],
                    )),
              ),
            ),
            AnimatedContainer(
              duration: const Duration(seconds: 10),
              curve: Curves.easeIn,
              width: 150,
              child: Card(
                margin: EdgeInsets.fromLTRB(15, 0, 10, 0),
                color: (_receiver == true &&
                        _client == false &&
                        _corprate == false)
                    ? _green
                    : _cardColor,
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(15.0),
                      topLeft: Radius.circular(15.0),
                      bottomLeft: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0)),
                ),
                child: InkWell(
                    highlightColor: Colors.green.withOpacity(0.5),
                    customBorder: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(15.0),
                          topLeft: Radius.circular(15.0),
                          bottomLeft: Radius.circular(15.0),
                          bottomRight: Radius.circular(15.0)),
                    ),
                    onTap: () {
                      setState(() {
                        _receiver = true;
                        _client = false;
                        _corprate = false;
                        servicePayer = globals.newTrip_recipient_name;
                        servicePayerID = "2";

                        globals.whomwillPay = servicePayer;
                        // sideLength == 50 ? sideLength = 100 : sideLength = 50;
                      });
                      // print(
                      //     "Service Payer will be $recipientNameController.text");
                    },
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 10),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(width: 5),
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(45.0),
                                        topLeft: Radius.circular(45.0),
                                        bottomLeft: Radius.circular(45.0),
                                        bottomRight: Radius.circular(0.0)),
                                    color: Colors.green[200]),
                                padding: EdgeInsets.all(5),
                                child: Icon(Icons.account_circle_rounded,
                                    size: 15, color: Colors.white),
                              ),
                              SizedBox(width: 5),
                              Container(
                                  //     padding: EdgeInsets.all(5),
                                  child: Flexible(
                                child: Text(
                                  " ${recipientNameController.text} \n Will Pay for the Service. ",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: (_receiver == true &&
                                              _client == false)
                                          ? _cardColor
                                          : Colors.black,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 11),
                                ),
                              )),
                              SizedBox(width: 5),
                            ]),
                        SizedBox(height: 10),
                      ],
                    )),
              ),
            ),

            //CORPRATE client
            if (_hasCoporateAccount == true)
              AnimatedContainer(
                duration: const Duration(seconds: 10),
                curve: Curves.easeIn,
                width: 150,
                child: Card(
                  margin: EdgeInsets.fromLTRB(15, 0, 10, 0),
                  color: (_receiver == false &&
                          _client == false &&
                          _corprate == true)
                      ? globals.primaryColor
                      : _cardColor,
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15.0),
                        topLeft: Radius.circular(15.0),
                        bottomLeft: Radius.circular(15.0),
                        bottomRight: Radius.circular(15.0)),
                  ),
                  child: InkWell(
                      highlightColor: Colors.green.withOpacity(0.5),
                      customBorder: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(15.0),
                            topLeft: Radius.circular(15.0),
                            bottomLeft: Radius.circular(15.0),
                            bottomRight: Radius.circular(15.0)),
                      ),
                      onTap: () {
                        setState(() {
                          _receiver = false;
                          _client = false;
                          _corprate = true;
                          servicePayer = "CORPRATE";
                          servicePayerID = "3";
                          // sideLength == 50 ? sideLength = 100 : sideLength = 50;
                          //globals.whomwillPay = servicePayer;
                          globals.whomwillPay = corporateName;
                        });
                        // print(
                        //     "Service Payer will be $recipientNameController.text");
                      },
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 10),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(width: 5),
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(45.0),
                                          topLeft: Radius.circular(45.0),
                                          bottomLeft: Radius.circular(45.0),
                                          bottomRight: Radius.circular(0.0)),
                                      // color: const Color(0xffdaf4f8)
                                      color: globals.primaryColor),
                                  padding: EdgeInsets.all(5),
                                  child: Icon(Icons.account_circle_rounded,
                                      size: 15, color: Colors.white),
                                ),
                                SizedBox(width: 5),
                                Container(
                                    child: Flexible(
                                  child: Text(
                                    " ${globals.corporateName}(Corporate) Bill. ",
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: (_receiver == false &&
                                                _client == false &&
                                                _corprate == true)
                                            ? _cardColor
                                            : Colors.black,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 11),
                                  ),
                                )),
                                SizedBox(width: 5),
                              ]),
                          SizedBox(height: 10),
                        ],
                      )),
                ),
              ),
          ],
        )));
  }

  displayAlertDialog(String title, String body)
  {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(title),
        content: Text(body),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Okay'),
            child: const Text('OKAY'),
          ),
          // TextButton(
          //   onPressed: () {
          //     acceptTrip(context);
          //   },
          //   child: const Text('Accept'),
          // ),
        ],
      ),
    );
  }

// === >  package input fields < ===
  _inputFields() {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 9.5), SizedBox(height: 6.5),
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.fromLTRB(25, 0, 0, 00),
          child: Text(
            "Package Details",
            style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.w300,
                fontSize: 15),
          ),
        ),
        SizedBox(height: 13.5),
        Align(
          alignment: Alignment(0.0, 0.0),
          child: _textField(
              label: 'Recipient name',
              hint: 'Enter recipient name',
              prefixIcon: Icon(Icons.account_circle_rounded,
                  size: 20, color: globals.primaryColor),
              controller: recipientNameController,
              focusNode: recipientNameFocusNode,
              width: width,
              locationCallback: (String value) {
                setState(() {
                  //  _startAddress = value;
                });
              }),
        ),
        SizedBox(height: 13),
        Align(
          alignment: Alignment(0.0, 0.0),
          child: _textField(
              label: 'Recipient Phone',
              hint: 'Enter recipient phone',
              prefixIcon: Icon(Icons.local_phone_rounded,
                  size: 20, color: globals.primaryColor),
              controller: recipientPhoneController,
              focusNode: recipientPhoneFocusNode,
              width: width,
              locationCallback: (String value) {
                setState(() {
                  //  _startAddress = value;
                });
              }),
        ),
        SizedBox(height: 13),
        Align(
          alignment: Alignment(0.0, 0.0),
          child: _textField(
              label: 'Package Description',
              hint: 'e.g. document',
              prefixIcon:
                  Icon(Icons.inventory, size: 20, color: globals.primaryColor),
              controller: packageDescriptionController,
              focusNode: startAddressFocusNode,
              width: width,
              locationCallback: (String value) {
                setState(() {
                  packageDetails = packageDescriptionController.text;
                });
              }),
        ),
        SizedBox(height: 13),
        // Divider(color: Colors.grey),
      ],
    );
  }
}

