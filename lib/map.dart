import 'dart:convert';
import 'dart:typed_data';
import 'package:ao_courier_client/widgets/navigationDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'config/secrets.dart';
import 'globals.dart' as globals;
import 'models/place_prediction.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

import 'dart:ui' as ui;

class MapView extends StatefulWidget {
  const MapView({Key? key}) : super(key: key);

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView>
    with AutomaticKeepAliveClientMixin<MapView> {
  late double tripEstimate = 0.0;
  late String clientEmail = "";
  late String billingAccount = "INDIVIDUAL";

  /** MAKE REQUEST ESTIMATE */
  checkTripEstimate(BuildContext context, String pickLat, String pickLng,
      String dropLat, String dropLng) async {
    String url = globals.BASE_URL;
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if (clientEmail == "" || billingAccount == "") {
      setState(() {
        clientEmail = "josephridge18@gmail.com";
        billingAccount = "INDIVIDUAL";
      });
    } else {
      setState(() {
        clientEmail = (globals.email);
        billingAccount = (globals.client_type);
      });
    }
    prefs.setString("userEmail", clientEmail);
  //  prefs.setString("billingAccount", billingAccount);

    print("Making api request....");
    var jsonObj;
    Map map = {
      'username': globals.username,
      'password': globals.password,
      "processingCode": "TRIPEST",
      "pickup_latitude": pickLat,
      "pickup_longitude": pickLng,
      "drop_off_latitude": dropLat,
      "drop_off_longitude": dropLng,
      "package_type": "SMALL",
      "trip_type": "EXPRESS",
      'channel': globals.channel,
    };
    print("MAP ==> $map");

    var resp = (await globals.apiRequest(url, map));
    print(resp);
    jsonObj = json.decode(resp);
    var responseCode = jsonObj['responseCode'];
    var responseDescription = jsonObj['responseDescription'];
    print("RESPONSE : $responseCode \n OBJE : $jsonObj");
    // tripEstimate = jsonObj['estimateCost'];
    print(" PRICE == > $tripEstimate");
     setState(() {
    tripEstimate = jsonObj['estimateCost']; } );
    globals.newTrip_cost = jsonObj['estimateCost'].toString();
    globals.newTrip_distance = jsonObj['tripDistance'];
    globals.newTrip_time_estimate = jsonObj['tripDuration'];
    // });
    globals.packageDetailsOK = true;
    _sharedPreferences.setString('pickUpLat', pickLat);
    _sharedPreferences.setString('pickUpLng', pickLng);
    _sharedPreferences.setString('dropOffLat', dropLat);
    _sharedPreferences.setString('dropOffLng', dropLng);
    _sharedPreferences.setString('userEmail', clientEmail);
    _sharedPreferences.setString("tripCost", tripEstimate.toString());
    // print(  "locations MAP == > $pickLat === > $pickLng ==> $dropLat ==> $dropLng .....");
  }

  // Get corporate accs
  getCorporateAccs() async
  {
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneNumber = prefs.getString('phoneNumber') ?? "";

    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'CLIENT_CORPORATES',
      'phone_number': phoneNumber,
      'channel': globals.channel,
    };
    resp = (await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj = json.decode(resp);
  responseCode = jsonObj['responseCode'];
  responseDescription = jsonObj['responseDescription'];

  if (responseCode == "000")
    {
      if (jsonObj['corporates'] != null)
        {
          billingAccount = "CORPORATE";
          prefs.setString('corporateName',jsonObj['corporates'][0]['corporateName']);
          globals.corporateName = jsonObj['corporates'][0]['corporateName'];
          prefs.setString('corporateID',jsonObj['corporates'][0]['corporateID']);
          prefs.setString('corporateEmail',jsonObj['corporates'][0]['email']);
        }
      else
        {
          prefs.setString('corporateName',"");
          globals.corporateName = "";
          prefs.setString('corporateID',"");
          prefs.setString('corporateEmail',"");
        }

    }
  else
    {
      prefs.setString('corporateName',"");
      globals.corporateName = "";
      prefs.setString('corporateID',"");
      prefs.setString('corporateEmail',"");
    }

    prefs.setString("billingAccount", billingAccount);

  }

  // * MAPS
  CameraPosition _initialLocation = CameraPosition(target: LatLng(0.0, 0.0));
  late String pos = '';
  String _latlngStart = '';
  String _currentAddress = '';

  // late Position _currentPosition;
  String _startAddress = '';
  List<dynamic> _startPlacesList = [];
  List<dynamic> _placesList = [];
  List<dynamic> _destinationPlacesList = [];
  Set<Marker> markers = {};
  String _destinationAddress = '';
  String? _placeDistance;
  String? _estimate;
  bool _estimate_pressed = false;
  late Marker destinationMarker;
  late Marker pickUpMarker;
  double currentLat = 0.0;
  double currentLng = 0.0;
  late String currentAddress = "";

//Markers
  late BitmapDescriptor pinLocationIcon;
  late dynamic start;
  late dynamic stop;

  //*Text Input Fields
  // Controllers
  final startAddressController = TextEditingController();
  final destinationAddressController = TextEditingController();
  final promoCodeController = TextEditingController();
  late GoogleMapController mapController;
  final startAddressFocusNode = FocusNode();
  final destinationAddressFocusNode = FocusNode();
  final promoCodeFocusNode = FocusNode();
  var currentFocus;
  double pickUpLat = 0;
  double pickUpLng = 0;
  late String pickUpAddress;
  late double dropOffLat = 0;
  late double dropOffLng = 0;
  late String dropOffAddress;

  //*main widget
  final _scaffoldKey = GlobalKey<ScaffoldState>();

//* auto-predict
  late int selectedItem;
  bool isSelected = false;
  bool dropIsSelected = false;

  //*Polylines
  late PolylinePoints polylinePoints;
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];

  //*API Post values
  String _startLatitude = "",
      _startLongitude = "",
      _destLatitude = "",
      _destLongitude = "";

  //*TEST
  // String start = "", stop = "";

  //!TO BE REMOVED WHEN CODE WORKS
  Color iconColor = Colors.amber.shade600;
  Color iconColorSelected = Colors.green.shade600;
  String currentSession = "";

  _textField({
    required TextEditingController controller,
    required FocusNode focusNode,
    required String label,
    required String hint,
    required double width,
    required Icon prefixIcon,
    required bool isPickUp,
    Widget? suffixIcon,
    required Function(String) locationCallback,
  }) {
    return Container(
      width: width * 0.8,
      child: TextField(
        onChanged: (value) {
          locationCallback(value);
          _getListOfProbablePlaces(value, context, isPickUp);
        },
        onTap: () {
          //  _getListOfProbablePlaces(controller.text, context, isPickUp);
          print(" ======= > ${controller.text}");
        },
        controller: controller,
        focusNode: focusNode,
        decoration: new InputDecoration(
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          labelText: label,
          filled: true,
          fillColor: Colors.white,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            borderSide: BorderSide(
              color: Colors.grey.shade400,
              width: 2,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10.0),
            ),
            borderSide: BorderSide(
              color: Colors.blue.shade300,
              width: 2,
            ),
          ),
          contentPadding: EdgeInsets.all(15),
          hintText: hint,
        ),
      ),
    );
  }

  _getCurrentPosition() async {
    final position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() async {
      _latlngStart = position.toString();
      currentLat = position.latitude;
      currentLng = position.longitude;
      // pos = position.toString();
      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(currentLat, currentLng),
            zoom: 18.0,
          ),
        ),
      );
      _initialLocation =
          await CameraPosition(target: LatLng(currentLat, currentLng));
    });
    _getAddress(currentLat, currentLng);
  }

  _current() async {
    await _getCurrentPosition();
    // session Key
    var selectedDate = DateTime.now().millisecondsSinceEpoch;
    globals.apiSessionKey = selectedDate.toString();
  }

  _getAddress(var lat, var lng) async {
    try {
      List<Placemark> p = await placemarkFromCoordinates(lat, lng);
      print(" ====== > Fetching addresses details ....");
      Placemark place = p[0];
      String placeName = place.name.toString();
      String placeLocality = place.locality.toString();
      setState(() {
        _currentAddress = "$placeName";
       // startAddressController.text = _currentAddress;
        _startAddress = _currentAddress;
      });
      print(
          "\n\n================== >  $placeName, $placeLocality < ========================\n\n");
    } catch (e) {
      print(e);
    }
  }

  _currentPositionButton() {
    return SafeArea(
      child: Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: const EdgeInsets.only(right: 10.0, bottom: 10.0),
          child: ClipOval(
            child: Material(
              color: Colors.orange.shade100, // button color
              child: InkWell(
                splashColor: Colors.orange, // inkwell color
                child: SizedBox(
                  width: 56,
                  height: 56,
                  child: Icon(Icons.my_location),
                ),
                onTap: () {
                  _getCurrentPosition();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _hamburgerMenu() {
    return SafeArea(
      child: Align(
        //  ..alignment: Alignment.topLeft,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ClipOval(
            child: Material(
              color: globals.accentColor, // button color
              child: InkWell(
                splashColor: Colors.orange, // inkwell color
                child: SizedBox(
                  width: 56,
                  height: 56,
                  child: Icon(Icons.my_location),
                ),
                onTap: () {
                  _getCurrentPosition();
                },
              ),
            ),
          ),
        ),
      ),
    );
  }

  _getListOfProbablePlaces(
      String placeInput, BuildContext context, bool isPickUp) async {
    String apiKey = Secrets.API_KEY;
    //TODO: Test both a
    https: //maps.googleapis.com/maps/api/place/autocomplete/json?input=flamingo&amp;key=AIzaSyDWGQewzXzlDpCvkxiYwpexgNuvGr8PLOk&amp;sessiontoken=1412&amp;components=country:ke&amp;location=-1.28273%2C36.82592&amp;radius=35000&amp;strictbounds=true
    String googleMapsUrl =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$placeInput&location=-1.28273%2C36.82592&radius=100000&strictbounds=true&types=establishment&key=$apiKey&sessiontoken=${globals.apiSessionKey}";
    // String googleMapsUrl =
    //     "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$placeInput&components=country:KE&key=$apiKey";
    print(" consuming places API....");
    var response = await http.get(Uri.parse(googleMapsUrl));
    var jsonResponse = jsonDecode(response.body)['predictions'];
    print("PLACES REPONSE == > ${jsonDecode(response.body)['description']}");
    if (response.statusCode == 200) {
      setState(() {
        _placesList = jsonDecode(response.body)['predictions'];
      });

      if (isPickUp == true) {
        setState(() {
          _startPlacesList = _placesList;
        });
      } else {
        setState(() {
          _destinationPlacesList = _placesList;
          print("====> $_destinationPlacesList");
        });
      }

      return jsonResponse
          .map((place) => PlacesPrediction.fromJson(place))
          .toList();
    }
  }

  getPlacesDetails(String placeID, String place, bool isPickUp) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    String apiKey = Secrets.API_KEY;
    String googleMapsUrl =
        "https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeID&key=$apiKey&sessiontoken=${globals.apiSessionKey}";
    print(" consuming placeDetails API....");
    print(" getting {{ $place }}  Details....");
    var response = await http.get(Uri.parse(googleMapsUrl));
    var jsonResponse =
        jsonDecode(response.body)['result']['geometry'] as Map<String, dynamic>;
    if (response.statusCode == 200) {
      if (isPickUp == true) {
        setState(() {
          pickUpLat = jsonResponse['location']['lat'];
          pickUpLng = jsonResponse['location']['lng'];
          pickUpAddress = place;
          _startLatitude = pickUpLat.toString();
          _startLongitude = pickUpLng.toString();

          globals.pickUpAddress = place;
          print("PICK LAT values == >  $pickUpLng");
          _sharedPreferences.setString("pickUpAddress", pickUpAddress);
        });
      } else {
        setState(() {
          globals.dropOffAddress = place;
          dropOffLat = jsonResponse['location']['lat'];
          dropOffLng = jsonResponse['location']['lng'];
          dropOffAddress = place;
          _destLatitude = dropOffLat.toString();
          _destLongitude = dropOffLng.toString();

          _sharedPreferences.setString("dropOffAddress", dropOffAddress);
        });
      }
    }
    print(
        "==> GEOMETRY $jsonResponse ===> LATITUDE : $pickUpLat ===> LONGITUDE : $pickUpLng");
    return jsonResponse;
  }

  _googleMaps() {
    return GoogleMap(
      markers: Set<Marker>.from(markers),
      mapType: MapType.normal,
      initialCameraPosition: _initialLocation,
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      zoomGesturesEnabled: true,
      zoomControlsEnabled: false,
      polylines: Set<Polyline>.of(polylines.values),
      onMapCreated: (GoogleMapController controller) {
        mapController = controller;
      },
    );
  }
  Future<Uint8List?> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))?.buffer.asUint8List();
  }
  // Method for calculating the distance between two places
  Future<bool> _calculateDistance (
      double pickLat, double pickLng, double dropLat, double dropLng) async {
    start = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(5, 5)), 'assets/images/start.png');

    stop = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(5, 5)), 'assets/images/stop.png');

    final Uint8List? startmarkerIcon = await getBytesFromAsset( 'assets/images/start.png', 50);
    final Uint8List? stopmarkerIcon = await getBytesFromAsset( 'assets/images/stop.png', 50);

    try {
      // List<Location> startPlacemark = await locationFromAddress(_startAddress);
      // List<Location> destinationPlacemark =
      //     await locationFromAddress(_destinationAddress);
      //
      // double startLatitude = _startAddress == _currentAddress
      //     ? currentLat
      //     : startPlacemark[0].latitude;
      //
      // double startLongitude = _startAddress == _currentAddress
      //     ? currentLng
      //     : startPlacemark[0].longitude;
      // double destinationLatitude = destinationPlacemark[0].latitude;
      // double destinationLongitude = destinationPlacemark[0].longitude;
      double startLatitude = pickLat;
      double startLongitude = pickLng;
      double destinationLatitude = dropLat;
      double destinationLongitude = dropLng;

      String startCoordinatesString = '($startLatitude, $startLongitude)';
      String destinationCoordinatesString =
          '($destinationLatitude, $destinationLongitude)';

      // // Start Location Marker
      Marker startMarker = Marker(
        markerId: MarkerId(startCoordinatesString),
        position: LatLng(pickLat, pickLng),
        infoWindow: InfoWindow(
          title: 'Pick Up Point .',
          snippet: "At $_startAddress",
        ),
         icon:  BitmapDescriptor.fromBytes(startmarkerIcon!) ,

      );

      // Destination Location Marker
      Marker destinationMarker = Marker(
        markerId: MarkerId(destinationCoordinatesString),
        position: LatLng(dropLat, dropLng),
        infoWindow: InfoWindow(
          title: 'DropOff Point',
          snippet: "At $_destinationAddress",
        ),
        icon: BitmapDescriptor.fromBytes(stopmarkerIcon!) ,

      );

      // Adding the markers to the list
      markers.add(startMarker);
      markers.add(destinationMarker);

      print(
        'START COORDINATES: ($startLatitude, $startLongitude)',
      );
      print(
        'DESTINATION COORDINATES: ($destinationLatitude, $destinationLongitude)',
      );
      setState(() {
        start = 'START COORDINATES: ($startLatitude, $startLongitude)';
        stop =
            'DESTINATION COORDINATES: ($destinationLatitude, $destinationLongitude)';
      });

      double miny = (startLatitude <= destinationLatitude)
          ? startLatitude
          : destinationLatitude;
      double minx = (startLongitude <= destinationLongitude)
          ? startLongitude
          : destinationLongitude;
      double maxy = (startLatitude <= destinationLatitude)
          ? destinationLatitude
          : startLatitude;
      double maxx = (startLongitude <= destinationLongitude)
          ? destinationLongitude
          : startLongitude;

      double southWestLatitude = miny;
      double southWestLongitude = minx;

      double northEastLatitude = maxy;
      double northEastLongitude = maxx;

      mapController.animateCamera(
        CameraUpdate.newLatLngBounds(
          LatLngBounds(
            northeast: LatLng(northEastLatitude, northEastLongitude),
            southwest: LatLng(southWestLatitude, southWestLongitude),
          ),
          100.0,
        ),
      );

      await _createPolylines(startLatitude, startLongitude, destinationLatitude,
          destinationLongitude);

      double totalDistance = 0.0;
      double estimate = 150.0;
      estimate = totalDistance * 20;

      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  // Create the polylines for showing the route between two places
  _createPolylines(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      Secrets.API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.transit,
    );

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('poly');
    Polyline polyline = Polyline(
      polylineId: id,
      color: globals.accentColor,
      points: polylineCoordinates,
      width: 3,
    );
    polylines[id] = polyline;
  }

  _pickUpLocation(var width) {
    return Column(children: <Widget>[
      _textField(
          controller: startAddressController,
          focusNode: startAddressFocusNode,
          label: 'Pickup',
          hint: 'Choose pickup point',
          width: width,
          prefixIcon: Icon(Icons.looks_one),
          suffixIcon: IconButton(
            icon: Icon(Icons.my_location),
            onPressed: () {
              _startAddress = _currentAddress;
            },
          ),
          isPickUp: true,
          locationCallback: (String value) {
            setState(() {
              _startAddress = value;
              var x = startAddressController.text;
              print("object === > $x");
            });
          }),
      /**COVERS FOR THE AUTOPREDICT FOR DROP PLACES */
      if (_startPlacesList.length != 0 && _startPlacesList.length != null)
        Container(
          height: 300,
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.9),
              backgroundBlendMode: BlendMode.darken),
          child: ListView.builder(
            itemCount: _startPlacesList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                child: ListTile(
                  leading: Icon(
                    Icons.place,
                    // color: iconColor,
                  ),
                  title: Container(
                    child: Text(
                      _startPlacesList[index]['description'],
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                      textAlign: TextAlign.justify,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    //   ),
                    // ],
                  ),
                  onTap: () {
                    setState(() {
                      selectedItem = index;
                      startAddressController.text =
                          _startPlacesList[selectedItem]['description'];
                      String item =
                          _startPlacesList[selectedItem]['description'];
                      _startAddress =
                          _startPlacesList[selectedItem]['description'];
                      pickUpAddress =
                          _startPlacesList[selectedItem]['description'];
                      String placeID =
                          _startPlacesList[selectedItem]['place_id'];
                      print(
                          "\n ********** selected Item is  : $item \n ********** selected Place_ID is  :$placeID");
                      // iconColor = Colors.green;
                      dropIsSelected = false;
                      // isSelected = true;
                      _startPlacesList.length = 0;
                      // searchIcon = Icons.cancel;
                      getPlacesDetails(placeID, item, true);
                    });
                  },
                ),
              );
            },
          ),
        ),
    ]);
  }

  _destinationLocation(var width) {
    return Column(children: <Widget>[
      _textField(
          controller: destinationAddressController,
          focusNode: destinationAddressFocusNode,
          label: 'Destination',
          hint: 'Choose destination',
          width: width,
          prefixIcon: Icon(Icons.looks_two),
          isPickUp: false,
          locationCallback: (String value) {
            setState(() {
              _destinationAddress = value;
            });
          }),
      /**COVERS FOR THE AUTOPREDICT FOR DROP PLACES */
      if (_destinationPlacesList.length != 0 &&
          _destinationPlacesList.length != null)
        Container(
          height: 300,
          decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.9),
              backgroundBlendMode: BlendMode.darken),
          child: ListView.builder(
            itemCount: _destinationPlacesList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                child: ListTile(
                  leading: Icon(
                    Icons.place,
                    color: iconColor,
                  ),
                  title: Container(
                    child: Text(
                      _destinationPlacesList[index]['description'],
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                      textAlign: TextAlign.justify,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    //   ),
                    // ],
                  ),
                  onTap: () {
                    setState(() {
                      selectedItem = index;
                      destinationAddressController.text =
                          _destinationPlacesList[selectedItem]['description'];
                      String item =
                          _destinationPlacesList[selectedItem]['description'];
                      _destinationAddress =
                          _destinationPlacesList[selectedItem]['description'];
                      pickUpAddress =
                          _destinationPlacesList[selectedItem]['description'];
                      String placeID =
                          _destinationPlacesList[selectedItem]['place_id'];
                      print(
                          "\n ********** selected Item is  : $item --\n ********** selected Place_ID is  :$placeID");
                      iconColor = Colors.green;
                      dropIsSelected = true;
                      // isSelected = true;
                      _destinationPlacesList.length = 0;

                      // searchIcon = Icons.cancel;
                      getPlacesDetails(placeID, item, false);
                    });
                  },
                ),
              );
            },
          ),
        )
    ]);
  }

  _requestDeliveryButton() {
    return ElevatedButton(
      onPressed: (_startAddress != '' && _destinationAddress != '')
          ? () async {
              _estimate_pressed = false;
              setState(() {
                globals.newTrip_pickup = _startAddress;
                globals.newTrip_dropoff = _destinationAddress;
                globals.newTrip_pickup_lat = _startLatitude;
                globals.newTrip_pickup_long = _startLongitude;
                globals.newTrip_dropoff_long = _destLongitude;
                globals.newTrip_dropoff_lat = _destLatitude;
                globals.promoCode=promoCodeController.text.trim();

                print("Proceed ==> " + globals.newTrip_pickup + _startAddress + " " +
                    globals.newTrip_dropoff + _destinationAddress + " " +
                    globals.newTrip_pickup_lat + _startLatitude + " " +
                    globals.newTrip_pickup_long + _startLongitude + " " +
                    globals.newTrip_dropoff_long + _destLongitude + " " +
                    globals.newTrip_dropoff_lat + _destLatitude);

              });
              globals.pageController.animateToPage(1,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInCubic);
            }
          : null,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          'Proceed '.toUpperCase(),
          style: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          ),
        ),
      ),
      style: ElevatedButton.styleFrom(
        primary: Colors.green,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
    );
  }

  _checkEstimateButton() {
    return ElevatedButton(
      onPressed: (_startAddress != '' && _destinationAddress != '')
          ? () async {
              startAddressFocusNode.unfocus();
              destinationAddressFocusNode.unfocus();
              _calculateDistance(pickUpLat, pickUpLng, dropOffLat, dropOffLng);
              print(" pick == > $pickUpLat");
              checkTripEstimate(
                  context,
                  pickUpLat.toString(),
                  pickUpLng.toString(),
                  dropOffLat.toString(),
                  dropOffLng.toString());
              setState(() {
                if (markers.isNotEmpty) markers.clear();
                if (polylines.isNotEmpty) polylines.clear();
                if (polylineCoordinates.isNotEmpty) polylineCoordinates.clear();
                _placeDistance = null;
                _estimate_pressed = true;
              });
            }
          : null,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          'Check Estimate'.toUpperCase(),
          style: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
          ),
        ),
      ),
      style: ElevatedButton.styleFrom(
        primary: globals.accentColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
    );
  }

  _refreshButton() {
    return InkWell(
        child: Column(

          mainAxisAlignment : MainAxisAlignment.center,
            children: [
      Center(child:IconButton(
        alignment: Alignment.bottomRight,
        splashRadius: 20,
        icon: const Icon(Icons.refresh,size:20, color:globals.accentColor),
        // tooltip: 'Refresh ',
        onPressed: () {
          //clear all set values
          setState(() {
            markers.clear();
            var selectedDate = DateTime.now().millisecondsSinceEpoch;
            globals.apiSessionKey = selectedDate.toString();
            _estimate_pressed = false;
            startAddressController.text = "";
            destinationAddressController.text = "";
            promoCodeController.text = "";
            polylineCoordinates.clear();
            tripEstimate = 0.0;
            // _volume += 10;
          });
        },
      )),
      Center(child:Text("Refresh",textAlign: TextAlign.center,style:TextStyle(color:globals.accentColor)))
    ]));
  }

  _promoCodeEntry() {
    var width = MediaQuery.of(context).size.width;
    return Container(
        width: width / 2.1,
        child: TextField(
          onTap: () {
            print(" ======= > ${promoCodeController.text.trim()}");
          },
          controller: promoCodeController,
          focusNode: promoCodeFocusNode,
          decoration: new InputDecoration(
            prefixIcon: Icon(Icons.redeem_outlined,
                size: 15, color: globals.primaryColor),
            labelText: "Enter Promo-Code",
            labelStyle: TextStyle(
               fontSize: 12,
            color: globals.primaryColor,
          ),
            // helperText: 'Enter PromoCode here',
            filled: true,
            fillColor: Colors.white,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30.0),
              ),
              borderSide: BorderSide(
                color: globals.accentColor2,
                width: 1.5,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30.0),
              ),
              borderSide: BorderSide(
                color: globals.accentColor,
                width: 1.5,
              ),
            ),
            contentPadding: EdgeInsets.all(8),
            hintText: "Enter PromoCode",
          ),
        ));
  }

  @override
  void initState() {
    super.initState();
    _current();
    getCorporateAccs();
  }

  _MapUI(width, isKeyboard, _scaffoldkey) {
    return Stack(
      children: [
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: 15),
            SafeArea(child: _hamburgerMenu()),
            Container(
              padding: EdgeInsets.fromLTRB(25, 7, 0, 00),
              child: Text(
                "Select Your Preferred Service ",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 13,
                    fontWeight: FontWeight.w600),
              ),
            ),
          ],
        ),
        _googleMaps(),
        SafeArea(
          child: Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white70,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                ),
                width: width * 0.9,
                child: SingleChildScrollView(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      if (!isKeyboard)
                        Text(
                          'Delivery fast & safe',
                          style: TextStyle(fontSize: 20.0),
                        ),
                      SizedBox(height: 15),
                      _pickUpLocation(width),
                      SizedBox(
                        height: 15,
                      ),
                      _destinationLocation(width),
                      SizedBox(height: 10),
                      // _dropOffWidget(width),
                      SizedBox(height: 10),
                      Visibility(
                        visible: true,
                        child: Text(
                          ///!get trip estimate here
                          'ksh ${tripEstimate.ceil()} /=',
                          style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(height: 5),

                      Visibility(
                        visible: _estimate_pressed == false ||
                                _startAddress == '' ||
                                _destinationAddress == ''
                            ? true
                            : false,
                        child: _checkEstimateButton(),
                      ),
                      SizedBox(height: 5),
                      Visibility(
                        visible: (_estimate_pressed == true) &&
                                (_startAddress != '' ||
                                    _destinationAddress != '')
                            ? true
                            : false,
                        child: _requestDeliveryButton(),
                      ),
                      Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [_refreshButton(), _promoCodeEntry()],
                      ))
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Container(
          width: 40,
          height: 40,
          margin: EdgeInsets.all(20),
          child: FloatingActionButton(
            elevation: 10,
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            child: const Icon(Icons.menu, color: Colors.white, size: 16),
            backgroundColor: globals.primaryColor,
            foregroundColor: globals.accentColor2,
          ),
        ),

        _currentPositionButton(),
      ],
    );
  }

  _mainMapViewUI(height, width, isKeyboard) {
    return Container(
      height: height,
      width: width,
      child: Scaffold(
          drawer: navigationDrawer(),
          key: _scaffoldKey,
          body: _MapUI(width, isKeyboard, _scaffoldKey)),
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: _mainMapViewUI(height, width, isKeyboard));
  }
}
