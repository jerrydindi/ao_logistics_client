import 'package:ao_courier_client/fragments/homePage.dart';
import 'package:ao_courier_client/fragments/promotionsPage.dart';
import 'package:ao_courier_client/fragments/tripsPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'fragments/profilePage.dart';
import 'routes/pageRoute.dart';
import 'package:ao_courier_client/globals.dart' as globals;

_getSharedPrefsValues()async{
  SharedPreferences _sharedPrefrences = await SharedPreferences.getInstance();
  globals.email = _sharedPrefrences.getString("email") ?? "";
  globals.name = _sharedPrefrences.getString("userName") ?? "";
}
class LandingPage extends StatelessWidget {
  const LandingPage({Key? key}) : super(key: key);
  static const appTitle = 'AO Logistics';


  @override
  Widget build(BuildContext context) {
    _getSharedPrefsValues();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      home: WillPopScope(
        child: homePage(),
        onWillPop: () async {
          Navigator.of(context).pop(false);
          (Route<dynamic> route) => false;

          return false;
        },
      ),
      routes: {
        pageRoutes.home: (context) => homePage(),
        pageRoutes.profile: (context) => ClientProfileDetails(),
        pageRoutes.trips: (context) => tripsPage(),
        pageRoutes.promotions: (context) => promotionsPage(),
      },
    );
  }
}
