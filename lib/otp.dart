import 'dart:convert';
import 'dart:math';

import 'package:ao_courier_client/globals.dart' as globals;
import 'package:ao_courier_client/landing.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import 'login.dart';

otpPersistNot() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('otpPersist', "0");
}

loginPersist() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('loginPersist', "1");
}

makeAPICall(String pwd, BuildContext context) async {
  try {
    showLoaderDialog(context);
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneNumber = (prefs.getString("phoneNumber"))!;
    //late String loggedIn = "0";
    print("OTP  validation .....");
    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'OTPVAL',
      'phone_number': phoneNumber,
      'otp': pwd,
      'channel': globals.channel,
    };
    print("map == > $map");
    resp = (await globals.apiRequest(url, map));
    print("resp == > $resp");
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];

    Fluttertoast.showToast(
        msg: responseDescription,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        // backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);

    Navigator.of(context, rootNavigator: true).pop('dialog');

    if (responseCode == "000") // success
    {
      otpPersistNot();
      loginPersist();

      // prefs.setString("loggedIn", "1");

      globals.name = jsonObj['client']['fullname'];
      prefs.setString('userName', globals.name);
      print("____ > > userName ${globals.name}");
      globals.phone = jsonObj['client']['phoneNumber'];
      globals.email = jsonObj['client']['email'];
      prefs.setString('email', globals.email);
      print("____ > > Email ${globals.email}");

      // String topicSpecific = 'ao_courier_client_' +
      //     globals.email.substring(0, globals.email.indexOf("@"));
      String topicSpecific = 'ao_courier_client_' + globals.phone;
      print(topicSpecific);
      await FirebaseMessaging.instance.subscribeToTopic(topicSpecific);
      await FirebaseMessaging.instance.subscribeToTopic("ao_courier_client");
      try {
        globals.corporateName = jsonObj['corporates']['corporateName'];
      } catch (e) {}
      globals.client_type = jsonObj['client']['clientType'];
      prefs.setString("clientType", globals.client_type);

      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => LandingPage()),
        (Route<dynamic> route) => false,
      );
    } else {
      //  Navigator.pop(context);
    }

  } catch (e) {
    //Handle all other exceptions
    Navigator.of(context, rootNavigator: true).pop('dialog');
    Fluttertoast.showToast(
        msg: "An error occured. Please try again later.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        // backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    print("apiRequest OTP" + e.toString());
  }
}

@override
showLoaderDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
        Container(
            margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class OTPPage extends StatefulWidget {
  _OTPPageState createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> with WidgetsBindingObserver {
  late String phonenumber = "";

  _getPersistenceValues() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    late var splitEmail = [];

    // setState(() {
    //     phonenumber = "254700000000";
    //    // splitEmail = phonenumber.split("@");
    //  });

    setState(() {
      phonenumber = (prefs.getString("phoneNumber"))!;
      // splitEmail = phonenumber.split("@");
    });

    // prefs.setString("email", phonenumber);
    print("phoneNumber ==== > $phonenumber");
  }

  void _showRatingAppDialog(BuildContext context) {
    final _ratingDialog = RatingDialog(
      ratingColor: Colors.amber,
      title: 'Rate the Trip',
      message: 'Leave us your feedback to help improve the service',
      //  image: Image.asset("assets/images/devs.jpg",
      //    height: 100,),
      submitButton: 'Submit',
      onCancelled: () => print('cancelled'),
      onSubmitted: (response) {
        print('rating: ${response.rating}, '
            'comment: ${response.comment}');

        if (response.rating < 3.0) {
          print('response.rating: ${response.rating}');
        } else {
          Container();
        }
      },
    );

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => _ratingDialog,
    );
  }

  TextEditingController valueOne = TextEditingController();
  TextEditingController valueTwo = TextEditingController();
  TextEditingController valueThree = TextEditingController();
  TextEditingController valueFour = TextEditingController();
  TextEditingController valueFive = TextEditingController();
  TextEditingController valueSix = TextEditingController();

  late String digitOne;
  late String digitTwo;
  late String digitThree;
  late String digitFour;
  late String digitFivr;
  late String digitSix;
  late String otpValue = "";

  String len = " 0";
  late bool hasError = false;

  late FocusNode pinOneFocusNode;
  late FocusNode pinTwoFocusNode;
  late FocusNode pinThreeFocusNode;
  late FocusNode pinFourFocusNode;
  late FocusNode pinFiveFocusNode;
  late FocusNode pinSixFocusNode;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    pinOneFocusNode = FocusNode();
    pinTwoFocusNode = FocusNode();
    pinThreeFocusNode = FocusNode();
    pinFourFocusNode = FocusNode();
    pinFiveFocusNode = FocusNode();
    pinSixFocusNode = FocusNode();
    _getPersistenceValues();
  }

  @override
  void dispose() {
    pinOneFocusNode.dispose();
    pinTwoFocusNode.dispose();
    pinThreeFocusNode.dispose();
    pinFourFocusNode.dispose();
    pinFiveFocusNode.dispose();
    pinSixFocusNode.dispose();
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      // user returned to our app
      print("AppLifecycleState.resumed");
    } else if (state == AppLifecycleState.inactive) {
      // app is inactive
      print("AppLifecycleState.inactive");
    } else if (state == AppLifecycleState.paused) {
      // user is about quit our app temporally
      print("AppLifecycleState.paused");
    } else if (state == AppLifecycleState.detached) {
      // app suspended (not used in iOS)
      print("AppLifecycleState.detached");
    }
  }

  nextField(String value, FocusNode focusNode) {
    if (value.length == 1) {
      // focusNode.requestFocus();
      FocusScope.of(context).nextFocus();
      // setState(() {
      //   len = value.length;
      // });
    }
  }

  _otpInputValues(TextEditingController position, FocusNode focusNode) {
    return TextFormField(
      //  maxLength: 1,
      focusNode: focusNode,
      keyboardType: TextInputType.number,
      controller: position,
      textAlign: TextAlign.center,
      // obscureText: true,
      onTap: () {
        position.text = "";
      },
      autofocus: true,
      onChanged: (value) {
        nextField(value, focusNode);
      },
      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),

      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        hintText: "0",
        hintStyle:
            TextStyle(color: Colors.grey[500], fontWeight: FontWeight.w500),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: new BorderSide(color: Colors.white)),
        labelStyle: TextStyle(color: globals.accentColor2),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: new BorderSide(color: Colors.greenAccent)),
      ),
    );
  }

  _otpMainUI(isKeyboard) {
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            // body: SingleChildScrollView(
            //   physics: NeverScrollableScrollPhysics(),
            //   child: ConstrainedBox(
            //     constraints: BoxConstraints(
            //       minWidth: MediaQuery.of(context).size.width,
            //       minHeight: MediaQuery.of(context).size.height,
            //     ),
            //     child: IntrinsicHeight(
            //       child: Column(
            //         mainAxisSize: MainAxisSize.max,
            //         children: <Widget>[
            body: Padding(
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Card(
                      color: globals.primaryColor,
                      elevation: 10,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Column(
                        children: [
                          Container(
                              margin: EdgeInsets.only(
                                top: 10,
                              ),
                              alignment: Alignment.center,
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                              child: Text(
                                'OTP Verification',
                                // style: Theme.of(context).textTheme.headline4,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 28,
                                ),
                              )),
                          Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.fromLTRB(10, 5, 10, 10),
                              child: Text(
                                'OTP Sent to ${phonenumber.substring(0, min(phonenumber.length, 8))}... ',
                                style: TextStyle(
                                  color: Colors.grey[400],
                                  fontWeight: FontWeight.w300,
                                ),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              PinCodeTextField(
                                pinBoxRadius: 15,
                                autofocus: true,
                                controller: valueOne,
                                hideCharacter: true,
                                highlight: true,
                                highlightColor: Colors.blue,
                                defaultBorderColor: globals.primaryColor,
                                hasTextBorderColor: globals.primaryColor,
                                highlightPinBoxColor: Colors.white,
                                maxLength: 6,
                                hasError: hasError,
                                //   maskCharacter: "😎",
                                onTextChanged: (text) {
                                  setState(() {
                                    hasError = false;
                                  });
                                },
                                onDone: (text) {
                                  print("DONE $text");
                                  print("DONE CONTROLLER ${valueOne.text}");
                                },
                                pinBoxWidth: 40,
                                pinBoxHeight: 54,
                                hasUnderline: false,
                                wrapAlignment: WrapAlignment.spaceAround,
                                pinBoxDecoration: ProvidedPinBoxDecoration
                                    .defaultPinBoxDecoration,
                                pinTextStyle: TextStyle(fontSize: 22.0),
                                pinTextAnimatedSwitcherTransition:
                                    ProvidedPinBoxTextAnimation
                                        .scalingTransition,
//                    pinBoxColor: Colors.green[100],
                                pinTextAnimatedSwitcherDuration:
                                    Duration(milliseconds: 300),
//                    highlightAnimation: true,
                                highlightAnimationBeginColor: Colors.black,
                                highlightAnimationEndColor: Colors.white12,
                                keyboardType: TextInputType.number,
                              ),
                            ],
                          ),
                          Container(
                              height: 70,
                              width: 150,
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              child: Card(
                                  elevation: 5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: TextButton(
                                    // textColor: Colors.white,
                                    // shape: RoundedRectangleBorder(
                                    //   side: BorderSide(
                                    //       color: globals.accentColor2, width: 2),
                                    // ),
                                    child: Text(
                                      ' Continue ',
                                      style: TextStyle(
                                          color: globals.primaryColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onPressed: () {
                                      // _showRatingAppDialog(context);
                                    //  showLoaderDialog(context);
                                      setState(() {
                                        // otpValue =
                                        //     "${valueOne.text}${valueTwo.text}${valueThree.text}${valueFour.text}${valueFive.text}${valueSix.text}";
                                        otpValue = "${valueOne.text}";
                                        globals.name = globals.name;
                                      });
                                      makeAPICall(otpValue, context);
                                      // Navigator.of(context)
                                      //     .pushReplacement(
                                      //   MaterialPageRoute(
                                      //       builder: (_) =>
                                      //           LandingPage()),
                                      // );
                                    },
                                  ))),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(builder: (_) => LoginPage()),
                              );
                            },
                            child: Text(
                              'Resend OTP? ',
                              style: TextStyle(
                                color: Colors.grey[300],
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                          ),
                          // SizedBox(
                          //   height: 10,
                          // )
                          // Container(
                          //   child: Text(
                          //       "${valueOne.text}${valueTwo.text} ${valueThree.text} ${valueFour.text}${valueFive.text} ${valueSix.text}"),
                          // )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    // if (!isKeyboard)
                    //   Positioned.fill(
                    //     child: Image.asset(
                    //       "assets/images/login.png",
                    //       fit: BoxFit.none,
                    //       alignment: Alignment.bottomRight,
                    //       height: 250,
                    //       scale: 1.4,
                    //     ),
                    //   ),
                  ],
                ))
            //         ],
            //       ),
            //     ),
            //   ),
            // )
            ));
  }

  @override
  Widget build(BuildContext context) {
    // _getPersistenceValues();
    final isKeyboard = MediaQuery.of(context).viewInsets.bottom != 0;
    return WillPopScope(
        child: _otpMainUI(isKeyboard),
        onWillPop: () async {
          return false;
        });
  }
}
