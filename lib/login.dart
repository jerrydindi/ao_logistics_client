import 'dart:convert';

// import 'package:ao_courier_client/otp.dart';
import 'package:ao_courier_client/globals.dart' as globals;
import 'package:ao_courier_client/otp.dart';
import 'package:ao_courier_client/register.dart';
import 'package:ao_courier_client/widgets/navigationDrawer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

var otp;

Widget _buildImage(String assetName, [double width = 350]) {
  return Image.asset('assets/$assetName', width: width);
}

otpPersist() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('otpPersist', "1");
}

makeAPICall(String phoneNumber, BuildContext context) async {
  try {
    showLoaderDialog(context);
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'OTPREQ',
      'phone_number': phoneNumber,
      'channel': globals.channel,
    };
    resp = (await globals.apiRequest(url, map));
    print(resp);
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];

    Fluttertoast.showToast(
        msg: responseDescription,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
    print("RESPOSNCE Description ==== > $responseDescription");
    print("RESPOSNCE Code ==== > $responseCode");

    Navigator.of(context, rootNavigator: true).pop('dialog');

    if (responseCode == "000") // change password
    {
      otpPersist();

      globals.phonenumber = phoneNumber;
      prefs.setString('phoneNumber', phoneNumber);

      // String topic_specific =
      //     'ao_courier_client_' + phoneNumber;
      // print(topic_specific);
      // await FirebaseMessaging.instance.subscribeToTopic(topic_specific);
      // // subscribe to main client notification topic
      // await FirebaseMessaging.instance.subscribeToTopic("ao_courier_client");

      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (_) => OTPPage()),
      );
    } else // invalid details
    {}
  } catch (e) {
    //Handle all other exceptions
    Navigator.of(context, rootNavigator: true).pop('dialog');
    Fluttertoast.showToast(
        msg: "An error occured. Please try again later.",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        // backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
    print("apiRequest Login" + e.toString());
  }
}

@override
showLoaderDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
        Container(
            margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class LoginPage extends StatelessWidget {
  void _showRatingAppDialog(BuildContext context) {
    final _ratingDialog = RatingDialog(
      ratingColor: Colors.amber,
      title: 'Rate the Trip',
      message: 'Leave us your feedback to help improve the service',
      submitButton: 'Submit',
      onCancelled: () => print('cancelled'),
      onSubmitted: (response) {
        print('rating: ${response.rating}, '
            'comment: ${response.comment}');
        if (response.rating < 3.0) {
          print('response.rating: ${response.rating}');
        } else {
          Container();
        }
      },
    );

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => _ratingDialog,
    );
  }

  TextEditingController phonenumberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
            body: Padding(
                padding: EdgeInsets.all(0),
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Card(
                      color: globals.primaryColor,
                      elevation: 10,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Column(
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              child: Text(
                                'Sign in',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                ),
                              )),
                          Container(
                            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                            child: TextField(
                              autofocus: true,
                              style: TextStyle(color: Colors.white70),
                              controller: phonenumberController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                labelText: 'Phone Number',
                                hintText: 'e.g 0712345678',
                                hintStyle: TextStyle(
                                    color: Colors.grey[300],
                                    fontWeight: FontWeight.w100),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12),
                                  borderSide:
                                      new BorderSide(color: Colors.grey),
                                ),
                                labelStyle: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15),
                                  borderSide:
                                      new BorderSide(color: Colors.white),
                                ),
                                prefixIcon: const Icon(
                                  Icons.phone_rounded,
                                  color: Colors.white,
                                ),
                                // color: globals.accentColor2)
                              ),
                            ),
                          ),
                          Container(
                              height: 70,
                              width: 150,
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              child: Card(
                                  elevation: 5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: TextButton(
                                    child: Text(
                                      ' Sign In ',
                                      style: TextStyle(
                                          color: globals.primaryColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onPressed: () {
                                    //  showLoaderDialog(context);
                                      //TODO :RETURN AFTER MAPS WORKS
                                      makeAPICall(
                                          phonenumberController.text.trim(),
                                          context);

                                      //     Navigator.of(context).pushReplacement(
                                      // MaterialPageRoute(
                                      //           builder: (_) => OTPPage()),
                                      //      );
                                    },
                                  ))),
                          Container(
                            // height: 70,
                            // width: 150,
                            alignment: Alignment.bottomRight,
                            padding: EdgeInsets.fromLTRB(10, 0, 15, 5),
                            child: TextButton(
                              onPressed: () {
                                Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(builder: (_) => Register()),
                                );
                              },
                              child: Text(
                                'No Account ? SignUp ',
                                style: TextStyle(
                                  color: Colors.grey[300],
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    /*Positioned.fill(
                      child: Image.asset(
                        "assets/images/login.png",
                        fit: BoxFit.none,
                        alignment: Alignment.bottomRight,
                        height: 350,
                      ),
                    ), */
                  ],
                ))));
  }
}
