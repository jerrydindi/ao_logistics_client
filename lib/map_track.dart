import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import '/config/secrets.dart'; // Stores the Google Maps API Key
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../globals.dart' as globals;

// AO-_COURIER-

class MapViewTrack extends StatefulWidget {
  @override
  _MapViewTrackState createState() => _MapViewTrackState();
}
//TODO: using shared preferences attain the pick and drop location coordinates and plot on map

class _MapViewTrackState extends State<MapViewTrack>
    with TickerProviderStateMixin {
  late CameraPosition _initialLocation =
      CameraPosition(target: LatLng(0.0, 0.0));
  late GoogleMapController mapController;

  final startAddressController = TextEditingController();
  final destinationAddressController = TextEditingController();

  final startAddressFocusNode = FocusNode();
  final desrinationAddressFocusNode = FocusNode();

  String _startAddress = '';

  //pick n drop points
  late double pickUpLat = 0;
  late double pickUpLng = 0;
  late String pickUpAddress = "";
  late double dropOffLat = 0;
  late double dropOffLng = 0;
  late String dropOffAddress = "";
  late double riderCurrentLat = 0;
  late double riderCurrentLng = 0;

  //Marker Icons
  late BitmapDescriptor pinLocationIcon;
  late dynamic start;
  late dynamic stop;

  Set<Marker> markers = {};
  late PolylinePoints polylinePoints;
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Timer initialization for the periodic calling of the makeApiCAll() method
  late Timer timer;
  late Stream stream;
  StreamController<double> controller = StreamController<double>.broadcast();
  StreamController _controller = StreamController.broadcast();
  late StreamSubscription streamSubscription;

  // == > Rider Details
  late final AnimationController _fabanimationController;
  late final Animation<double> _fabAnimationProgress;
  bool _open = false;
  late String _assignedRider = "Assigned Rider";
  late Icon riderIcon = Icon(Icons.moped);
  late String riderEmail = "";
  late String riderName = "";
  late String motimertocycleNumberPlate = "";

  // == > Trip Details
  late AnimationController _animationController;
  late String _title = "Locating Rider..";
  late String tripStatus = "";
  late String tripStartTime = "";
  late String tripEndTime = "";
  late String now = "";
  late bool _showLinearProgress = false;
  late Color statusColor = globals.primaryColor;

  //removed when cleaning
  late var _count = 0;

  late LatLngBounds bounds = LatLngBounds(
    northeast: LatLng(0.0, 0.0),
    southwest: LatLng(0.0, 0.0),
  );

  late LatLng centerBounds = LatLng(
      (bounds.northeast.latitude + bounds.southwest.latitude) / 2,
      (bounds.northeast.longitude + bounds.southwest.longitude) / 2);

//   custom pins
  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 4.5),
        'assets/images/ic_biker.png');
  }

  /* Method to get STREAM OF DATA TO TRACK N UPDATE RIDER LOCATION **START** */
  late Timer trackrider;

  _getRiderCurrentLocation(BuildContext context) async {
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    late double riderLat = 0, riderLng = 0;
    var jsonObj;
    print("tracking rider ....");

    Map map = {
      'username': globals.username,
      'password': globals.password,
      "processingCode": "RDRCURLOC",
      "trip_id": globals.newTrip_tripID,
      "channel": globals.channel
    };

    resp = (await globals.apiRequest(url, map));
    print(resp);
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];

    riderLat = double.parse(jsonObj['currentLatitude']);
    riderLng = double.parse(jsonObj['currentLongitude']);
    setState(() {
      riderCurrentLat = riderLat;
      riderCurrentLng = riderLng;
    });
  }

  currentRiderLocation() {
    trackrider = Timer.periodic(
        Duration(seconds: 10), (Timer t) => _getRiderCurrentLocation(context));
  }

// == > get trip status
  _makeApiCall(BuildContext context) async {
    String url = globals.BASE_URL;
    print("fetching trip Status ....");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      tripStatus = "FULFILLED";
      _count++;
    });
    print(" tripStatus === > $tripStatus");
    print(" fetchFrequency === > $_count");
    var jsonObj;
    Map map = {
      'username': globals.username,
      'password': globals.password,
      "processingCode": "TRIPDET",
      "trip_id": globals.newTrip_tripID,
      'channel': globals.channel,
    };
    switch (tripStatus) {
      case "FULFILLED":
        setState(() {
          markers.remove(markers.firstWhere(
              (Marker marker) => marker.markerId.value == "Starting Point"));
          print("Marker : == > ${markers}");
          _title = "Rider has arrived ... ";
          _showLinearProgress = false;
          trackrider.cancel();
          timer.cancel();
          statusColor = globals.accentColor2;
        });
        Future.delayed(Duration(seconds: 20), () {
          print("Executed after 20 seconds");
          String msg = "Rider has ended,awaiting payment";
          _showRatingAppDialog(context);
          showMessage("Delivery Complete", msg, context);
        });
        break;
      case "DISPUTED":
        setState(() {
          _title = "Trip Disputed... ";
          _showLinearProgress = false;
          statusColor = globals.accentColor;
        });
        timer
            .cancel(); // cancel Trip track status when trip has been disputed ...
        break;
      case "REQUESTED":
        setState(() {
          _title = "Locating Rider .... ";
          _showLinearProgress = true;
          statusColor = globals.accentColor2;
        });
        break; // The switch statement must be told to exit, or it will execute every case.
      case "ALLOCATED":
        setState(() {
          _title = "Locating Rider .... ";
          _showLinearProgress = true;
          statusColor = globals.primaryColor;
        });
        break;
      case "ACCEPTED":
        setState(() {
          _title = "Trip will start shortly ... ";
          _showLinearProgress = true;
          riderCurrentLat = pickUpLat;
          riderCurrentLng = pickUpLng;
          statusColor = globals.primaryColor;
        });
        _showRiderLocation();
        break;
      case "STARTED":
        setState(() {
          _title = "Trip in started and in progress .... ";
          _count++;
          _showLinearProgress = true;
        });
        currentRiderLocation();
        updateRiderLocation();
        _controller.sink.add(_count);
        _controller.sink.add(riderCurrentLat);
        _controller.sink.add(riderCurrentLng);
        print("counter == > $_count");
        statusColor = Colors.green;
        break;
      default:
        print('choose a different number!');
    }
  }

// = > show Rider Location - basically start location willbe riders default loc..
  _showRiderLocation() {
    Marker startMarker = Marker(
      markerId: MarkerId("startCoordinatesString"),
      position: LatLng(riderCurrentLat, riderCurrentLng),
      infoWindow: InfoWindow(
        title: 'Rider location',
        snippet: _startAddress,
      ),
      icon: pinLocationIcon,
      zIndex: 2,
      flat: false,
      anchor: Offset(0.5, 1.5),
    );

    setState(() {
      markers.add(startMarker);
    });

    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(riderCurrentLat, riderCurrentLng),
          zoom: 18.0,
        ),
      ),
    );
  }

// == > update rider position on map
  updateRiderLocation() {
    Marker startMarker = Marker(
      markerId: MarkerId("startCoordinatesString"),
      position: LatLng(riderCurrentLat, riderCurrentLng),
      infoWindow: InfoWindow(
        title: 'Rider location',
        snippet: _startAddress,
      ),
      icon: pinLocationIcon,
      zIndex: 2,
      flat: false,
      anchor: Offset(0.5, 1.5),
    );

    setState(() {
      markers.add(startMarker);
    });

    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(riderCurrentLat, riderCurrentLng),
          zoom: 18.0,
        ),
      ),
    );
  }

  //== > stream Function
  _listenToStream() {
    stream = _controller.stream;
    streamSubscription = stream.listen((event) {
      print("Stream LatLng== > $riderCurrentLat == > $riderCurrentLng ");
      print("Stream Count== > $_count");
    }, onError: (error) {
      print("Stream Error : ${error.toString()}!!");
    }, onDone: () {
      print("Stream Closed !!");
    });
  }

  periodicApiCall() {
    timer = Timer.periodic(
        Duration(seconds: 2), (Timer t) => _makeApiCall(context));
    switch (tripStatus) {
      case "FULFILLED":
        setState(() {
          timer.cancel();
          trackrider.cancel();
        });
        break;
      case "DISPUTED":
        setState(() {
          timer.cancel();
        });
        // cancel Trip track status when trip has been disputed ...
        break;
      case "REQUESTED":
        setState(() {});
        break; // The switch statement must be told to exit, or it will execute every case.
      case "ALLOCATED":
        setState(() {});
        break;
      case "ACCEPTED":
        setState(() {});
        _showRiderLocation();
        break;
      case "STARTED":
        setState(() {});
        break;
      default:
        print('continuing frequent fetching ...');
    }
  }

  _cancelStreamSubscription() {
    streamSubscription.cancel();
  }

/* Method to get STREAM OF DATA TO TRACK N UPDATE RIDER LOCATION  **END** */

// Rate rider
  void _showRatingAppDialog(BuildContext context) {
    final _ratingDialog = RatingDialog(
      ratingColor: Colors.amber,
      title: 'Rate the Rider',
      message: 'How was the interaction with the rider?',
      submitButton: 'Submit',
      onCancelled: () {
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        print('cancelled');
      },
      onSubmitted: (response) {
        print('rating: ${response.rating}, '
            'comment: ${response.comment}');
        if (response.rating < 3.0) {
          print('response.rating: ${response.rating}');
        } else {
          Container();
        }
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
      },
    );

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => _ratingDialog,
    );
  }

  //trip ended
  void showMessage(String title, String message, BuildContext context) {
    BuildContext dialogContext;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [Text(message)],
              ),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  //  acceptTrip(context);
                  Navigator.pop(dialogContext);
                  _showRatingAppDialog(context);
                  globals.packageDetailsOK = false;
                  globals.pageController.animateToPage(0,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInCubic);
                },
                child: const Text('OK'),
              ),
            ],
          );
        });
  }

/*  Methods to sort out pick up and drop locations ad plots polyline between the two points **START ** */

  //Plot polyline between start and drop points
  _pathToBeTravelled() async {
    start = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(25, 25)), 'assets/images/start.png');

    stop = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(25, 25)), 'assets/images/stop.png');

    print("setting start and drop locations ....");
    //TODO : ADD SHARED PREFS to get start and drop locations
    setState(() {
      pickUpLat = double.parse(globals.newTrip_pickup_lat);
      pickUpLng = double.parse(globals.newTrip_pickup_long);
      dropOffLat = double.parse(globals.newTrip_dropoff_lat);
      dropOffLng = double.parse(globals.newTrip_dropoff_long);
      dropOffAddress = globals.newTrip_dropoff;
      pickUpAddress = globals.newTrip_pickup;
      _initialLocation =
          CameraPosition(target: LatLng(pickUpLat, pickUpLng), zoom: 15);
    });
    print(
        " Pick == > $pickUpLat, $pickUpLng  Drop == > $dropOffLat, $dropOffLng ");
    Marker startMarker = Marker(
        markerId: MarkerId("Starting Point"),
        position: LatLng(pickUpLat, pickUpLng),
        infoWindow: InfoWindow(
          title: 'Pick Up Point.',
          snippet: ("$pickUpAddress"),
        ),
        zIndex: 1,
        flat: true,
        icon: start);

    Marker stopMarker = Marker(
        markerId: MarkerId("Destination"),
        position: LatLng(dropOffLat, dropOffLng),
        infoWindow: InfoWindow(
          title: 'Destination.',
          snippet: ("$dropOffAddress"),
        ),
        zIndex: 1,
        flat: true,
        icon: stop);

    setState(() {
      markers.add(startMarker);
      markers.add(stopMarker);
    });
    print("Markers == > $markers");
    _calculateBounds(pickUpLat, pickUpLng, dropOffLat, dropOffLng);
  }

  // Create the polylines for showing the route between two places
  _createPolylines(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      Secrets.API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.transit,
    );

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('poly');
    Polyline polyline = Polyline(
      polylineId: id,
      color: globals.accentColor,
      points: polylineCoordinates,
      width: 3,
    );
    polylines[id] = polyline;
  }

  // Method for calculating the distance between two places
  Future<bool> _calculateBounds(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    // String apiKey = Secrets.API_KEY;
    try {
      print(
        'START COORDINATES: ($startLatitude, $startLongitude)',
      );
      print(
        'DESTINATION COORDINATES: ($destinationLatitude, $destinationLongitude)',
      );

      // Calculating to check that the position relative
      // to the frame, and pan & zoom the camera accordingly.
      double miny = (startLatitude <= destinationLatitude)
          ? startLatitude
          : destinationLatitude;
      double minx = (startLongitude <= destinationLongitude)
          ? startLongitude
          : destinationLongitude;
      double maxy = (startLatitude <= destinationLatitude)
          ? destinationLatitude
          : startLatitude;
      double maxx = (startLongitude <= destinationLongitude)
          ? destinationLongitude
          : startLongitude;

      double southWestLatitude = miny;
      double southWestLongitude = minx;

      double northEastLatitude = maxy;
      double northEastLongitude = maxx;
      print(
          "BOUNDS == > $northEastLatitude, $northEastLongitude, $southWestLatitude, $southWestLongitude");
      // Accommodate the two locations within the
      // camera view of the map
      setState(() {
        bounds = LatLngBounds(
          northeast: LatLng(northEastLatitude, northEastLongitude),
          southwest: LatLng(southWestLatitude, southWestLongitude),
        );
        centerBounds = LatLng(
            (bounds.northeast.latitude + bounds.southwest.latitude) / 2,
            (bounds.northeast.longitude + bounds.southwest.longitude) / 2);
      });

      mapController.animateCamera(
        CameraUpdate.newLatLngBounds(
          bounds,
          150.0,
        ),
      );
      mapController.moveCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: centerBounds, zoom: 18)));
      //wait _createPolylines(pickUpLat, pickUpLng, dropOffLat, dropOffLng);
      await _createPolylines(startLatitude, startLongitude, destinationLatitude,
          destinationLongitude);

      return true;
    } catch (e) {
      print(e);
    }
    return false;
  }

  // Methods to sort out pick up and drop locations ad plots poluline between the two points **END **
//top progress bar
  _progressBar() {
    return (Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        if (_showLinearProgress == true)
          LinearProgressIndicator(
            value: _animationController.value,
            backgroundColor: Colors.green[500],
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            // semanticsLabel: 'Linear progress indicator',
          ),
      ],
    ));
  }

  //Current-Time
  _currentTime(final String currentTime) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        // SizedBox(width: 5),
        Container(
          padding: EdgeInsets.fromLTRB(25, 5, 35, 5),
          child: Text(
            "Estimated Delivery Time : $currentTime ",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: globals.accentColor2,
                fontSize: 22,
                fontWeight: FontWeight.w300),
          ),
        ),
      ],
    ));
  }

  String estimateDeliveryTime() {
    var today = DateTime.now();
    var thirtyMinsFromNow = today.add(const Duration(minutes: 30));
    String formattedTime = DateFormat('kk:mm:a').format(thirtyMinsFromNow);
    return formattedTime;
  }

  _aogroupRider(String riderName, String riderPhoneNumber, String numberPlate) {
    return Scrollbar(
        isAlwaysShown: true,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Divider(indent: 1, endIndent: 1, height: 10, thickness: 25),
            Container(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Rider Details",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  // child: Text(currentTime),
                  child: Text(
                    "Name  :  ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  child: Text(
                    riderName,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  // child: Text(currentTime),
                  child: Text(
                    "Contact  :  ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 00),
                  child: Text(
                    riderPhoneNumber,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  // child: Text(currentTime),
                  child: Text(
                    "Motorcycle PlateNumber:  ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 00),
                  child: Text(
                    numberPlate,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Divider(
                indent: 1,
                endIndent: 1,
                height: 10,
                thickness: 1.5,
                color: globals.accentColor),
            SizedBox(height: 5),
            Text(
              "Communicate to Rider",
              //  textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  color: globals.accentColor,
                  fontWeight: FontWeight.w300),
            ),
            SizedBox(height: 10),
            InkWell(
                onTap: () {
                  print("== > tapped ..");
                },
                child: Card(
                    margin: EdgeInsets.only(left: 50, right: 50),
                    color: globals.accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 5,
                    child: Container(
                      width: 130,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(10),
                              // child: Text(currentTime),
                              child: Icon(Icons.call,
                                  color: Colors.white, size: 20)),
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: Text(
                              "Call Rider",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                    ))),
            SizedBox(height: 15),
            Divider(
                indent: 1,
                endIndent: 1,
                height: 10,
                thickness: 1.5,
                color: globals.primaryColor),
            SizedBox(height: 15),
            Text(
              "Leave  Rider a message ..",
              //  textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 18,
                  color: globals.primaryColor,
                  fontWeight: FontWeight.w300),
            ),
            SizedBox(height: 10),
            TextField(
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  hintText: 'hi Rider...',
                ),
                style: TextStyle(
                    fontWeight: FontWeight.w300, color: globals.accentColor2)),
            SizedBox(height: 10),
            InkWell(
                onTap: () {
                  print("== >text tapped ..");
                },
                child: Card(
                    margin: EdgeInsets.only(left: 50, right: 50),
                    color: globals.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 5,
                    child: Container(
                      width: 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(10),
                              // child: Text(currentTime),
                              child: Icon(Icons.message,
                                  color: Colors.white, size: 20)),
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: Text(
                              "Text Rider",
                              //textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                    ))),
            SizedBox(height: 10),
            Text(
                " Disclaimer : Kindly note that the rider might not respond to calls upon trip start, however, you can leave him a message. Thank you.",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 12,
                    color: globals.accentColor2)),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.cancel, color: globals.accentColor),
            ),
          ],
        ));
  }

  _openTapToCloseFAB() {
    setState(() {
      _assignedRider = "Assigned Rider";
      riderIcon = Icon(Icons.moped);
      ;
    });
  }

  _tripID(String tripID) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 5, 0, 5),
          // child: Text(currentTime),
          child: Text(
            "Trip ID : ",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
          child: Text(
            tripID,
            textAlign: TextAlign.right,
            style: TextStyle(
                color: Colors.grey[200],
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    ));
  }

  _packageDetails(String packageDetails) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 5, 0, 00),
          // child: Text(currentTime),
          child: Text(
            "Package  :  ",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade600,
                fontWeight: FontWeight.w300),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 0, 00),
          child: Text(
            "packageDetails",
            textAlign: TextAlign.right,
            style: TextStyle(
                color: globals.primaryColor,
                fontSize: 15,
                fontWeight: FontWeight.w300),
          ),
        ),
      ],
    ));
  }

  _servicePriceEstimate(String priceEstimate) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 0, 0, 0),
          child: Text(
            "Delivery Costs :",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade600,
                fontWeight: FontWeight.w300),
          ),
        ),
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: Text(
                "Ksh. ",
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: globals.primaryColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w300),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                priceEstimate,
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 15,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ],
        ),
      ],
    ));
  }

  _paymentDetails(String personToPay) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 0, 0, 0),
          child: Text(
            "Payment will be done by  ",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade600,
                fontWeight: FontWeight.w300),
          ),
        ),
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                "$personToPay .",
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: globals.primaryColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ],
        ),
      ],
    ));
  }

  _orderProgress(String pickUpPoint, String dropOffPoint) {
    return (Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Pick UP Point :  ",
          textAlign: TextAlign.left,
          maxLines: 1,
          softWrap: false,
          style: TextStyle(
              color: globals.primaryColor,
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.fromLTRB(0, 0, 40, 25),
          child: Text(
            "$pickUpPoint",
            textAlign: TextAlign.left,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            softWrap: false,
            style: TextStyle(
                color: globals.primaryColor,
                fontSize: 14,
                fontWeight: FontWeight.w300),
          ),
        ),
        SizedBox(height: 45),
        Text(
          "DropOff Point :  ",
          textAlign: TextAlign.left,
          maxLines: 1,
          softWrap: false,
          style: TextStyle(
              color: globals.accentColor,
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.fromLTRB(0, 0, 40, 0),
          child: Text(
            "$dropOffPoint",
            //textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: globals.accentColor,
                fontSize: 14,
                fontWeight: FontWeight.w300),
          ),
        ),
      ],
    ));
  }

  _startDropIcons() {
    final String startIcon = 'assets/images/start.png';
    final String stopIcon = 'assets/images/stop.png';
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.fromLTRB(15.0, 5.0, 10.0, 10.0),
          width: 25.0,
          height: 25.0,
          child: Image(image: AssetImage(startIcon)),
        ),
        Container(
          height: 45.0,
          margin: const EdgeInsets.fromLTRB(15.0, 0.0, 10.0, 0.0),
          child: VerticalDivider(
            color: Colors.black,
            width: 80,
            thickness: 1.3,
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(15.0, 15.0, 10.0, 5.0),
          width: 25.0,
          height: 25.0,
          child: Image(image: AssetImage(stopIcon)),
        ),
      ],
    );
  }

  _packageSection() {
    return Column(children: <Widget>[
      Container(
          padding: EdgeInsets.fromLTRB(25, 10, 0, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Package Description   ",
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w400),
              ),
            ],
          )
          // child: Text(currentTime),
          ),
      Divider(
        indent: 25,
        endIndent: 25,
      ),
      //   -- > package details
      Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
        child: _packageDetails(globals.newTrip_package),
      ),
      //   -- > price
      Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
          child: _servicePriceEstimate(globals.newTrip_cost)),
      Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: _paymentDetails(globals.whomwillPay)),
    ]);
  }

  void _riderDetailsDialog(String title, String message, BuildContext context) {
    BuildContext dialogContext;
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return AlertDialog(
            title: Text(title, style: TextStyle(fontWeight: FontWeight.w400)),
            content:
                _aogroupRider("RiderName_", "RiderPhoneNumber", "KBDC 123V"),
            actions: <Widget>[],
          );
        });
  }

  @override
  void initState() {
    _fabanimationController = AnimationController(
      vsync: this,
      value: _open ? 1.0 : 0.0,
      duration: const Duration(milliseconds: 250),
    );
    _fabAnimationProgress = CurvedAnimation(
        curve: Curves.fastOutSlowIn,
        reverseCurve: Curves.easeOutQuad,
        parent: _fabanimationController);

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    )..addListener(() {
        setState(() {});
      });
    _animationController.repeat(reverse: true);
    setCustomMapPin();
    trackrider.cancel();
    super.initState();
    _pathToBeTravelled();
    periodicApiCall();
    _listenToStream();
    streamSubscription.resume();
  }

  @override
  void dispose() {
    super.dispose();
    _cancelStreamSubscription();
    _animationController.dispose();
    _fabanimationController.dispose();
  }

  void _toggle() {
    setState(() {
      _open = !_open;
      if (_open) {
        //    _closeTapToCloseFAB();
        _fabanimationController.forward();
      } else {
        _openTapToCloseFAB();
        _fabanimationController.reverse();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    now = estimateDeliveryTime();
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    globals.packageDetailsOK = true;
    if (globals.packageDetailsOK) {
      return Container(
          height: height * 0.8,
          width: width,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: new AppBar(
              automaticallyImplyLeading: true,
              backgroundColor: Colors.white,
              title: new Text(_title,
                  style: TextStyle(
                      color: globals.accentColor2,
                      fontWeight: FontWeight.w400)),
              bottom: PreferredSize(
                  preferredSize: Size(double.infinity, 1.0),
                  child: _progressBar()),
            ),
            body: SlidingUpPanel(
              backdropOpacity: 0.3,
              backdropEnabled: true,
              parallaxEnabled: true,
              // parallaxOffset: .5,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(18.0),
                  topRight: Radius.circular(18.0)),
              panel: IntrinsicHeight(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Divider(
                      color: Colors.grey[300],
                      height: 30,
                      thickness: 5,
                      indent: 130,
                      endIndent: 130,
                    ),
                    //  -- > estimate time of arrival
                    Container(
                        child: Row(
                      children: <Widget>[
                        Container(child: _currentTime(now)),
                      ],
                    )),
                    Divider(
                      indent: 25,
                      endIndent: 25,
                    ),
                    Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                          // SizedBox(width: 10),
                          Container(
                            // padding: EdgeInsets.fromLTRB(25, 10, 35, 10),
                            padding: EdgeInsets.fromLTRB(25, 5, 35, 5),
                            child: Text(
                              "Order Status : ",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 35, 5),
                              child: Card(
                                  color: statusColor,
                                  elevation: 4,
                                  child: Container(
                                    width: 100,
                                    padding: EdgeInsets.all(5),
                                    child: Text(
                                      tripStatus,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ))),
                          SizedBox(width: 10),
                        ])),
                    // -- > order progress
                    Container(
                      //  margin: const EdgeInsets.fromLTRB(15.0, 7.0, 10.0, 10.0),
                      child: Row(
                        children: <Widget>[
                          Row(
                            children: [
                              //indicators
                              IntrinsicHeight(
                                child: _startDropIcons(),
                              ),
                              // --- > TripStart n drop points
                              Row(
                                children: <Widget>[
                                  Container(
                                      child: _orderProgress(
                                          pickUpAddress, dropOffAddress)),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Card(
                      elevation: 5,
                      color: Colors.grey.shade200,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: _packageSection(),
                    ),

                    FloatingActionButton.extended(
                        backgroundColor: globals.accentColor,
                        onPressed: () {
                          _toggle();
                          _riderDetailsDialog(
                              "RIDER DETAILS ", "WElcome", context);
                          // _closeTapToCloseFAB();
                        },
                        icon: riderIcon,
                        label: Text(_assignedRider)),
                  ],
                ),
              ),

              //behind the sliding panel
              body: Stack(
                children: <Widget>[
                  GoogleMap(
                    markers: Set<Marker>.from(markers),
                    initialCameraPosition: _initialLocation,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false,
                    mapType: MapType.normal,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: false,
                    polylines: Set<Polyline>.of(polylines.values),
                    onMapCreated: (GoogleMapController controller) {
                      mapController = controller;
                      mapController
                          .moveCamera(CameraUpdate.newLatLngBounds(bounds, 50));
                    },
                  ),
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ClipOval(
                            child: Material(
                              color: Colors.blue.shade100, // button color
                              child: InkWell(
                                splashColor: Colors.blue, // inkwell color
                                child: SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: Icon(Icons.add),
                                ),
                                onTap: () {
                                  mapController.animateCamera(
                                    CameraUpdate.zoomIn(),
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          ClipOval(
                            child: Material(
                              color: Colors.blue.shade100, // button color
                              child: InkWell(
                                splashColor: Colors.blue, // inkwell color
                                child: SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: Icon(Icons.remove),
                                ),
                                onTap: () {
                                  mapController.animateCamera(
                                    CameraUpdate.zoomOut(),
                                  );
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ));
    } else {
      return Container(
          height: height,
          width: width,
          child: Scaffold(
              // drawer: navigationDrawer(),
              body: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: 15),
                  FloatingActionButton(
                    elevation: 15,
                    highlightElevation: 10,
                    onPressed: () {
                      // _scaffoldkey.currentState!.openDrawer();
                      Scaffold.of(context).openDrawer();
                      // navigationDrawer()/;
                    },
                    child: const Icon(
                      Icons.menu,
                      color: Colors.white,
                    ),
                    backgroundColor: globals.primaryColor,
                    // )
                  ),
                  /* === >  Select Service text < === */
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 7, 0, 00),
                    child: Text(
                      "Trip Request Status Page .... ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 13,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ],
              ),
              Center(
                child: Text('Complete Package Request on Previous Page'),
              )
            ],
          )));
    }
  }
}
