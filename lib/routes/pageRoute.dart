import 'dart:async';
import 'dart:convert';

import 'package:ao_courier_client/fragments/homePage.dart';
import 'package:ao_courier_client/fragments/profilePage.dart';
import 'package:ao_courier_client/fragments/tripsPage.dart';
import 'package:ao_courier_client/fragments/promotionsPage.dart';

class pageRoutes{
  static const String home = homePage.routeName;
  static const String profile = ClientProfileDetails.routeName;
  static const String trips = tripsPage.routeName;
  static const String promotions = promotionsPage.routeName;
}