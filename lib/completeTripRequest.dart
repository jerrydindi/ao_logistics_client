import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:url_launcher/url_launcher.dart';
import '/config/secrets.dart'; // Stores the Google Maps API Key
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../globals.dart' as globals;
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'dart:math' show cos, sqrt, asin;
import 'dart:ui' as ui;
import '../globals.dart';
import 'fragments/homePage.dart';

class LoadTrackRiderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: "", home: MapViewTrack());
  }
}

class MapViewTrack extends StatefulWidget {
  @override
  _MapViewTrackState createState() => _MapViewTrackState();
}

class _MapViewTrackState extends State<MapViewTrack>
    with TickerProviderStateMixin {
  late CameraPosition _initialLocation =
      CameraPosition(target: LatLng(0.0, 0.0));
  late GoogleMapController mapController;

  late Position _currentPosition;
  String _currentAddress = '';

  final startAddressController = TextEditingController();
  final destinationAddressController = TextEditingController();

  final startAddressFocusNode = FocusNode();
  final desrinationAddressFocusNode = FocusNode();

  String _startAddress = '';
  String _destinationAddress = '';
  String? _placeDistance;
  String? _estimate;
  bool _estimate_pressed = false;

  //pick n drop points
  double pickUpLat = 0;
  double pickUpLng = 0;
  double dropOffLat = 0;
  double dropOffLng = 0;
  late double riderCurrentLat = 0;
  late double riderCurrentLng = 0;

  //Marker Icons
  late BitmapDescriptor pinLocationIcon;
  late dynamic start;
  late dynamic stop;
  late Uint8List? riderpinmarkerIcon;

  Set<Marker> markers = {};
  late PolylinePoints polylinePoints;
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // Timer initialization for the periodic calling of the makeApiCAll() method
  late Timer timer;
  late Stream stream;
  StreamController<double> controller = StreamController<double>.broadcast();
  StreamController _controller = StreamController.broadcast();
  late StreamSubscription streamSubscription;

  // == > Rider Details
  late final AnimationController _fabanimationController;
  late final Animation<double> _fabAnimationProgress;
  bool _open = false;
  late String _assignedRider = "Contact Rider";
  late Icon riderIcon = Icon(Icons.moped);
  late String riderEmail = "";
  late String riderName = "";
  late String hintMessage = "";
  late String riderPhone = "";
  late String motimertocycleNumberPlate = "";
  late String riderCurrentLocation = "";

  // == > Trip Details
  late AnimationController _animationController;
  late String _title = "Locating Rider..";
  late String tripStatus = globals.newTrip_status;
  late String tripStartTime = "";
  late String tripEndTime = "";
  late String now = "";
  late bool _showLinearProgress = false;
  late Color statusColor = globals.primaryColor;

  //slidePanel Details
  late String packageDescription = "";
  late String packageDeliveryCost = "";
  late String servicePayer = "";
  late String pickUpAddress = "";
  late String dropOffAddress = "";
  late String paymentBy = "";
  late String tripID = "";
  late String message = "";
  TextEditingController messageController = TextEditingController();

  late String clientComment = "";
  late double clientRating = 0;
  late String servicePayerID = "0";

  //removed when cleaning
  late var _count = 0;
  late bool _hasCoporateAccount = false;

  late LatLngBounds bounds = LatLngBounds(
    northeast: LatLng(0.0, 0.0),
    southwest: LatLng(0.0, 0.0),
  );

  late LatLng centerBounds = LatLng(
      (bounds.northeast.latitude + bounds.southwest.latitude) / 2,
      (bounds.northeast.longitude + bounds.southwest.longitude) / 2);

//   custom pins
  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        'assets/images/ic_biker.png');

    riderpinmarkerIcon =
        await getBytesFromAsset('assets/images/ic_biker.png', 70);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    servicePayerID = prefs.getString("servicePayerID")!;
    setState(() {
      servicePayer = prefs.getString("payerDetails")!;
      pickUpLat = double.parse(globals.newTrip_pickup_lat);
      pickUpLng = double.parse(globals.newTrip_pickup_long);
      dropOffLat = double.parse(globals.newTrip_dropoff_lat);
      dropOffLng = double.parse(globals.newTrip_dropoff_long);
      if (servicePayerID == "1") {
        servicePayer = globals.name;
      } else if (servicePayerID == "2") {
        servicePayer = globals.newTrip_recipient_name;
      } else if (servicePayerID == "3") {
        servicePayer = globals.corporateName;
      }
      if (globals.corporateName == null) {
        _hasCoporateAccount = false;
      } else {
        _hasCoporateAccount = true;
      }
    });
  }

  Future<Uint8List?> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        ?.buffer
        .asUint8List();
  }

  /* Method to get STREAM OF DATA TO TRACK N UPDATE RIDER LOCATION **START** */

  // == > get trip status
  _makeApiCall(BuildContext context) async {
    String url = globals.BASE_URL;
    print("fetching trip Status ....");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    tripID = prefs.getString('tripCode')!;
    var jsonObj;
    Map map = {
      'username': globals.username,
      'password': globals.password,
      "processingCode": "TRIPDET",
      "trip_id": tripID,
      'channel': globals.channel,
    };
    var resp = (await globals.apiRequest(url, map));
    // print("== >R $resp");
    jsonObj = json.decode(resp);
    var responseCode = jsonObj['responseCode'];
    var responseDescription = jsonObj['responseDescription'];
    if (responseCode == "000") // success
    {
      setState(() {
        pickUpLat = double.parse(jsonObj['pickup_latitude'].toString());
        pickUpLng = double.parse(jsonObj['pickup_longitude'].toString());
        dropOffLat = double.parse(jsonObj['dropoff_latitude'].toString());
        dropOffLng = double.parse(jsonObj['dropoff_longitude'].toString());
      });
      tripID = jsonObj['responseDescription'];
      packageDescription = jsonObj['package_description'];
      packageDeliveryCost = jsonObj['trip_cost'].toString();
      pickUpAddress = jsonObj['pickup_address'];
      dropOffAddress = jsonObj['dropoff_address'];
      riderName = jsonObj['rider_name'];
      paymentBy = jsonObj['paymentBy'];
      if (jsonObj['rider_location'] == null) {
        riderCurrentLocation = "0.0,0.0";
      } else {
        riderCurrentLocation = jsonObj['rider_location'];
      }
      hintMessage = "Hello $riderName , ...";
      riderEmail = jsonObj['rider_email'];
      motimertocycleNumberPlate = jsonObj['numberPlate'];

      ///TODO: link to server
      riderPhone = jsonObj['rider_phone'];
      tripStatus = jsonObj['status'];
    }
    print(
        "Trip Details == > $tripID  == > $packageDescription ==> $packageDeliveryCost ==> $servicePayer ==> $pickUpAddress RIDER == > $riderEmail == > $riderName ==> $riderPhone ==> $tripStatus");

    switch (tripStatus) {
      case "FULFILLED":
        setState(() {
          markers.remove(markers.firstWhere(
              (Marker marker) => marker.markerId.value == "Starting Point"));
          print("Marker : == > ${markers}");
          _title = "Rider has arrived ... ";
          _showLinearProgress = false;
          timer.cancel();
          statusColor = globals.accentColor2;
        });
        if (globals.requestedTrip == true) {
          Future.delayed(Duration(seconds: 20), () {
            print("Executed after 20 seconds");
            String msg = "Rider has ended,awaiting payment";
            _rateAOService(context);
          });
        }

        break;
      case "DISPUTED":
        setState(() {
          _title = "Trip Disputed... ";
          _showLinearProgress = false;
          statusColor = globals.accentColor;
        });
        timer
            .cancel(); // cancel Trip track status when trip has been disputed ...
        break;
      case "REQUESTED":
        setState(() {
          _title = "Locating Rider .... ";
          _showLinearProgress = true;
          statusColor = globals.accentColor2;
        });
        break; // The switch statement must be told to exit, or it will execute every case.
      case "ALLOCATED":
        setState(() {
          _title = "Locating Rider .... ";
          _showLinearProgress = true;
          statusColor = globals.primaryColor;
        });
        break;
      case "ACCEPTED":
        setState(() {
          _title = "Trip will start shortly ... ";
          _showLinearProgress = true;
          riderCurrentLat = pickUpLat;
          riderCurrentLng = pickUpLng;
          statusColor = globals.primaryColor;
        });
        _showRiderLocation();
        break;
      case "STARTED":
        print(
            'rider position : ${jsonObj['rider_location']} == > ${riderCurrentLocation[0]} ');
        print(
            "LAT LANG == > ${globals.newTrip_pickup_lat}, ${globals.newTrip_pickup_long} ");
        setState(() {
          _title = "Trip has started .... ";
          //TODO: == > update with currentlatlng response from api
          riderCurrentLocation = jsonObj['rider_location'];
          if (riderCurrentLocation == "0.00,0.00") {
            setState(() {
              riderCurrentLat = pickUpLat;
              riderCurrentLng = pickUpLng;
              print(
                  'if 0.0 rider position : $riderCurrentLat== > $riderCurrentLng ');
            });
          } else {
            riderCurrentLat = double.parse(riderCurrentLocation[0]);
            riderCurrentLng = double.parse(riderCurrentLocation[1]);
          }
          _count++;
          _showLinearProgress = true;
        });
        _controller.sink.add(_count);
        _controller.sink.add(riderCurrentLat);
        _controller.sink.add(riderCurrentLng);
        print("counter == > $_count");
        statusColor = Colors.green;
        updateRiderLocation();
        break;
      default:
        print('choose a different number!');
    }
  }

  //send rider message
  _sendRiderText(tripID, message) async {
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'CLNTCHAT',
      'email': email,
      'tripId': tripID,
      'message': message,
      'channel': globals.channel,
    };
    resp = (await globals.apiRequest(url, map));
    print(resp);
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];

    // Navigator.pop(context);  ==> dont add this in first route
    Fluttertoast.showToast(
        msg: responseDescription,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
  }

// = > show Rider Location - basically start location willbe riders default loc..
  _showRiderLocation() {
    Marker startMarker = Marker(
      markerId: MarkerId("startCoordinatesString"),
      position: LatLng(riderCurrentLat, riderCurrentLng),
      infoWindow: InfoWindow(
        title: 'Rider location',
        snippet: _startAddress,
      ),
      icon: BitmapDescriptor.fromBytes(riderpinmarkerIcon!),
      zIndex: 2,
      flat: false,
      anchor: Offset(0.5, 1.5),
    );

    setState(() {
      markers.add(startMarker);
    });

    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(riderCurrentLat, riderCurrentLng),
          zoom: 18.0,
        ),
      ),
    );
  }

// == > update rider position on map
  updateRiderLocation() {
    Marker startMarker = Marker(
      markerId: MarkerId("startCoordinatesString"),
      position: LatLng(riderCurrentLat, riderCurrentLng),
      infoWindow: InfoWindow(
        title: 'Rider location',
        snippet: _startAddress,
      ),
      icon: pinLocationIcon,
      zIndex: 2,
      flat: false,
      anchor: Offset(0.5, 1.5),
    );

    setState(() {
      markers.add(startMarker);
    });

    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(riderCurrentLat, riderCurrentLng),
          zoom: 18.0,
        ),
      ),
    );
  }

  //== > stream Function
  _listenToStream() {
    stream = _controller.stream;
    streamSubscription = stream.listen((event) {
      print("Stream LatLng== > $riderCurrentLat == > $riderCurrentLng ");
      print("Stream Count== > $_count");
    }, onError: (error) {
      print("Stream Error : ${error.toString()}!!");
    }, onDone: () {
      print("Stream Closed !!");
    });
  }

  periodicApiCall() async {
    timer = Timer.periodic(
        Duration(seconds: 10), (Timer t) => _makeApiCall(context));
    switch (tripStatus) {
      case "FULFILLED":
        setState(() {
          timer.cancel();
        });
        break;
      case "DISPUTED":
        setState(() {
          timer.cancel();
        });
        // cancel Trip track status when trip has been disputed ...
        break;
      case "REQUESTED":
        setState(() {});
        break; // The switch statement must be told to exit, or it will execute every case.
      case "ALLOCATED":
        setState(() {});
        break;
      case "ACCEPTED":
        setState(() {});
        _showRiderLocation();
        break;
      case "STARTED":
        setState(() {});
        break;
      default:
        print('continuing frequent fetching ...');
    }
  }

  _cancelStreamSubscription() {
    streamSubscription.pause();
  }

  //Rate rider requ
  _rateRider(BuildContext context, tripID, message, riderEmail, stars) async {
    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", responseCode = "";
    var jsonObj;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map map = {
      'username': globals.username,
      'password': globals.password,
      'processingCode': 'RATERDR',
      'email': riderEmail,
      'tripId': tripID,
      "start": stars,
      'feedback': message,
      'channel': globals.channel,
    };
    resp = (await globals.apiRequest(url, map));
    print(resp);
    jsonObj = json.decode(resp);
    responseCode = jsonObj['responseCode'];
    responseDescription = jsonObj['responseDescription'];

    // Navigator.pop(context);  ==> dont add this in first route
    Fluttertoast.showToast(
        msg: responseDescription,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
  }

/* Method to get STREAM OF DATA TO TRACK N UPDATE RIDER LOCATION  **END** */

// Rate rider
  _rateAOService(BuildContext context) => showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
          content: ListView(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 10),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Rate the Rider',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(height: 10),
                  RatingBar.builder(
                    initialRating: 1,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 35,
                    itemPadding: EdgeInsets.symmetric(horizontal: 5.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      print(rating);
                      setState(() {
                        clientRating = rating;
                      });
                    },
                  ),
                  SizedBox(height: 10),
                  Text(
                    'How was the interaction with the rider ? ',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                  ),
                  SizedBox(height: 3),
                  TextField(
                    autofocus: true,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontWeight: FontWeight.w400),
                    controller: messageController,
                    decoration: InputDecoration(
                      hintText: "hi team AO, ....",
                      hintStyle: TextStyle(
                          color: Colors.grey[500], fontWeight: FontWeight.w500),
                      labelStyle: TextStyle(color: Colors.blueGrey),
                    ),
                  ),
                ],
              ),
            ],
          ),
          actions: [
            TextButton(
                onPressed: () {
                  setState(() {
                    clientComment = messageController.text;
                  });
                  print(
                      "Comments == > $clientComment, $riderEmail, $clientRating");
                  _rateRider(
                      context, tripID, clientComment, riderEmail, clientRating);
                  if (Navigator.canPop(context)) {
                    Navigator.pop(context);
                  }

                  globals.pageController.animateToPage(0,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInCubic);

                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => homePage()),
                  );
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: globals.primaryColor),
                )),
            SizedBox(height: 10)
          ],
        ),
      );

  void _showRatingAppDialog(BuildContext context) {
    final _ratingDialog = RatingDialog(
      ratingColor: Colors.amber,
      title: 'Rate the Rider',
      message: 'How was the interaction with the rider?',
      submitButton: 'Submit',
      onCancelled: () {
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        print('cancelled');
      },
      onSubmitted: (response) {
        print('rating: ${response.rating}, '
            'comment: ${response.comment}');
        _rateRider(
            context, tripID, response.comment, riderEmail, response.rating);
        if (response.rating < 3.0) {
          print('response.rating: ${response.rating}');
        } else {
          Container();
        }
        if (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
      },
    );

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => _ratingDialog);
  }

  //trip ended
  void showMessage(String title, String message, BuildContext context) {
    BuildContext dialogContext;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [Text(message)],
              ),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.pop(dialogContext);
                  _showRatingAppDialog(context);
                  globals.packageDetailsOK = false;
                  globals.pageController.animateToPage(0,
                      duration: Duration(milliseconds: 500),
                      curve: Curves.easeInCubic);
                },
                child: const Text('OK'),
              ),
            ],
          );
        });
  }

/*  Methods to sort out pick up and drop locations ad plots polyline between the two points **START ** */

  //Plot polyline between start and drop points
  _pathToBeTravelled() async {
    start = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(5, 5)), 'assets/images/start.png');

    stop = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(5, 5)), 'assets/images/stop.png');

    final Uint8List? startmarkerIcon =
        await getBytesFromAsset('assets/images/start.png', 70);
    final Uint8List? stopmarkerIcon =
        await getBytesFromAsset('assets/images/stop.png', 70);

    print("setting start and drop locations ....");
    //TODO : ADD SHARED PREFS to get start and drop locations
    setState(() {
      dropOffAddress = globals.newTrip_dropoff;
      pickUpAddress = globals.newTrip_pickup;
      _initialLocation =
          CameraPosition(target: LatLng(pickUpLat, pickUpLng), zoom: 15);
    });
    print(
        " Complete trip Pick == >$pickUpAddress @ $pickUpLat, $pickUpLng  Drop == > $dropOffAddress @ $dropOffLat, $dropOffLng   ");
    Marker startMarker = Marker(
        markerId: MarkerId("Starting Point"),
        position: LatLng(pickUpLat, pickUpLng),
        infoWindow: InfoWindow(
          title: 'Pick Up Point : $pickUpAddress".',
          snippet: ("$pickUpAddress"),
        ),
        zIndex: 1,
        flat: true,
        icon: BitmapDescriptor.fromBytes(startmarkerIcon!));

    Marker stopMarker = Marker(
        markerId: MarkerId("Destination"),
        position: LatLng(dropOffLat, dropOffLng),
        infoWindow: InfoWindow(
          title: 'Destination at : $dropOffAddress.',
          snippet: ("$dropOffAddress"),
        ),
        zIndex: 1,
        flat: true,
        icon: BitmapDescriptor.fromBytes(stopmarkerIcon!));

    setState(() {
      markers.add(startMarker);
      markers.add(stopMarker);
    });
    print("Markers == > $markers");
    _calculateBounds(pickUpLat, pickUpLng, dropOffLat, dropOffLng);
  }

  // Create the polylines for showing the route between two places
  _createPolylines(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      Secrets.API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      travelMode: TravelMode.transit,
    );

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    PolylineId id = PolylineId('poly');
    Polyline polyline = Polyline(
      polylineId: id,
      color: globals.primaryColor,
      points: polylineCoordinates,
      width: 4,
    );
    polylines[id] = polyline;
  }

  // Method for calculating the distance between two places
  Future<bool> _calculateBounds(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    // String apiKey = Secrets.API_KEY;
    try {
      print(
        'START COORDINATES: ($startLatitude, $startLongitude)',
      );
      print(
        'DESTINATION COORDINATES: ($destinationLatitude, $destinationLongitude)',
      );

      double miny = (startLatitude <= destinationLatitude)
          ? startLatitude
          : destinationLatitude;
      double minx = (startLongitude <= destinationLongitude)
          ? startLongitude
          : destinationLongitude;
      double maxy = (startLatitude <= destinationLatitude)
          ? destinationLatitude
          : startLatitude;
      double maxx = (startLongitude <= destinationLongitude)
          ? destinationLongitude
          : startLongitude;

      double southWestLatitude = miny;
      double southWestLongitude = minx;

      double northEastLatitude = maxy;
      double northEastLongitude = maxx;
      print(
          "BOUNDS == > $northEastLatitude, $northEastLongitude, $southWestLatitude, $southWestLongitude");
      // Accommodate the two locations within the
      // camera view of the map
      setState(() {
        bounds = LatLngBounds(
          northeast: LatLng(northEastLatitude, northEastLongitude),
          southwest: LatLng(southWestLatitude, southWestLongitude),
        );
        centerBounds = LatLng(
            (bounds.northeast.latitude + bounds.southwest.latitude) / 2,
            (bounds.northeast.longitude + bounds.southwest.longitude) / 2);
      });

      mapController.animateCamera(
        CameraUpdate.newLatLngBounds(
          bounds,
          150.0,
        ),
      );
      mapController.moveCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: centerBounds, zoom: 18)));
      await _createPolylines(startLatitude, startLongitude, destinationLatitude,
          destinationLongitude);

      return true;
    } catch (e) {
      print("ERROR == > $e");
    }
    await _createPolylines(startLatitude, startLongitude, destinationLatitude,
        destinationLongitude);
    return false;
  }

  // Methods to sort out pick up and drop locations ad plots poluline between the two points **END **
//top progress bar
  _progressBar() {
    return (Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        if (_showLinearProgress == true)
          LinearProgressIndicator(
            value: _animationController.value,
            backgroundColor: Colors.green[500],
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            // semanticsLabel: 'Linear progress indicator',
          ),
      ],
    ));
  }

  //Current-Time
  _currentTime(final String currentTime) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 5, 5, 5),
          child: Text(
            "Estimated Delivery Time : $currentTime HRS",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: globals.accentColor2,
                fontSize: 20,
                fontWeight: FontWeight.w300),
          ),
        ),
      ],
    ));
  }

  String estimateDeliveryTime() {
    var today = DateTime.now();
    var thirtyMinsFromNow = today.add(const Duration(minutes: 30));
    String formattedTime = DateFormat('kk:mm').format(thirtyMinsFromNow);
    return formattedTime;
  }

  _aogroupRider(String riderName, String riderPhoneNumber, String numberPlate) {
    return Scrollbar(
        isAlwaysShown: true,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Divider(indent: 1, endIndent: 1, height: 10, thickness: 25),
            Container(
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Rider Details",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  // child: Text(currentTime),
                  child: Text(
                    "Name  :  ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  child: Text(
                    riderName,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  // child: Text(currentTime),
                  child: Text(
                    "Contact  :  ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 00),
                  child: Text(
                    riderPhoneNumber,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 00),
                  // child: Text(currentTime),
                  child: Text(
                    "Motorcycle PlateNumber: ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(5, 5, 0, 00),
                  child: Text(
                    numberPlate,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: globals.primaryColor,
                        fontSize: 12,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Divider(
                indent: 1,
                endIndent: 1,
                height: 10,
                thickness: 1.5,
                color: globals.accentColor),
            SizedBox(height: 5),
            Container(
              alignment: Alignment.topCenter,
              child: Text(
                "Communicate to Rider",
                style: TextStyle(
                    fontSize: 18,
                    color: globals.accentColor,
                    fontWeight: FontWeight.w300),
              ),
            ),
            SizedBox(height: 10),
            InkWell(
                onTap: () {
                  print("== > tapped ..");
                  launch("tel://" + "+" + globals.riderPhone_number);
                },
                child: Card(
                    margin: EdgeInsets.only(left: 50, right: 50),
                    color: globals.accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 5,
                    child: Container(
                      width: 130,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(10),
                              // child: Text(currentTime),
                              child: Icon(Icons.call,
                                  color: Colors.white, size: 20)),
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: Text(
                              "Call Rider",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                    ))),
            SizedBox(height: 15),
            Divider(
                indent: 1,
                endIndent: 1,
                height: 10,
                thickness: 1.5,
                color: globals.primaryColor),
            SizedBox(height: 15),

            SizedBox(height: 10),
            SizedBox(height: 10),
            /* InkWell(
                onTap: () {
                  setState(() {
                    message = messageController.text.trim();
                  });
                  _sendRiderText(tripID, message);
                },
                child: Card(
                    margin: EdgeInsets.only(left: 50, right: 50),
                    color: globals.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    elevation: 5,
                    child: Container(
                      width: 80,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.all(10),
                              // child: Text(currentTime),
                              child: Icon(Icons.message,
                                  color: Colors.white, size: 20)),
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: Text(
                              "Text Rider",
                              //textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ],
                      ),
                    ))), */
            SizedBox(height: 10),
            Text(
                " Disclaimer : Kindly note that the rider might not respond to calls upon trip start, however, you can leave him a message. Thank you.",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 12,
                    color: globals.accentColor2)),

          ],
        ));
  }

  _openTapToCloseFAB() {
    setState(() {
      _assignedRider = "Assigned Rider";
      riderIcon = Icon(Icons.moped);
      ;
    });
  }

  _tripID(String tripID) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 5, 0, 5),
          // child: Text(currentTime),
          child: Text(
            "Trip ID : ",
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
          child: Text(
            tripID,
            textAlign: TextAlign.right,
            style: TextStyle(
                color: Colors.grey[200],
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
      ],
    ));
  }

  _packageDetails(String packageDetails) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 5, 0, 00),
          // child: Text(currentTime),
          child: Text(
            "Package  :  ",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade600,
                fontWeight: FontWeight.w300),
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(5, 5, 0, 00),
          child: Text(
            packageDetails,
            textAlign: TextAlign.right,
            style: TextStyle(
                color: globals.primaryColor,
                fontSize: 15,
                fontWeight: FontWeight.w300),
          ),
        ),
      ],
    ));
  }

  _servicePriceEstimate(String priceEstimate) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 0, 0, 0),
          child: Text(
            "Delivery Costs :",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade600,
                fontWeight: FontWeight.w300),
          ),
        ),
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
              child: Text(
                "Ksh. ",
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: globals.primaryColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w300),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                priceEstimate,
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: globals.primaryColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ],
        ),
      ],
    ));
  }

  _paymentDetails(String personToPay) {
    return (Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(25, 0, 0, 0),
          child: Text(
            "Payment will be done by  ",
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade600,
                fontWeight: FontWeight.w300),
          ),
        ),
        Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(
                "${globals.newTrip_recipient_name} .",
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: globals.primaryColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ],
        ),
      ],
    ));
  }

  _orderProgress(String pickUpPoint, String dropOffPoint) {
    return (Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Pick UP Point :  ",
          textAlign: TextAlign.left,
          maxLines: 1,
          softWrap: false,
          style: TextStyle(
              color: globals.primaryColor,
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.fromLTRB(0, 0, 40, 25),
          child: Text(
            "$pickUpPoint",
            textAlign: TextAlign.left,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            softWrap: false,
            style: TextStyle(
                color: globals.primaryColor,
                fontSize: 14,
                fontWeight: FontWeight.w300),
          ),
        ),
        SizedBox(height: 45),
        Text(
          "DropOff Point :  ",
          textAlign: TextAlign.left,
          maxLines: 1,
          softWrap: false,
          style: TextStyle(
              color: globals.accentColor,
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        Container(
          width: 200,
          padding: EdgeInsets.fromLTRB(0, 0, 40, 0),
          child: Text(
            "$dropOffPoint",
            //textAlign: TextAlign.left,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: globals.accentColor,
                fontSize: 14,
                fontWeight: FontWeight.w300),
          ),
        ),
      ],
    ));
  }

  _startDropIcons() {
    final String startIcon = 'assets/images/start.png';
    final String stopIcon = 'assets/images/stop.png';
    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.fromLTRB(15.0, 5.0, 10.0, 10.0),
          width: 25.0,
          height: 25.0,
          child: Image(image: AssetImage(startIcon)),
        ),
        Container(
          height: 45.0,
          margin: const EdgeInsets.fromLTRB(15.0, 0.0, 10.0, 0.0),
          child: VerticalDivider(
            color: Colors.black,
            width: 80,
            thickness: 1.3,
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(15.0, 15.0, 10.0, 5.0),
          width: 25.0,
          height: 25.0,
          child: Image(image: AssetImage(stopIcon)),
        ),
      ],
    );
  }

  _packageSection() {
    return Column(children: <Widget>[
      Container(
          padding: EdgeInsets.fromLTRB(25, 10, 0, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Package Description   ",
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.w400),
              ),
            ],
          )),
      Divider(
        indent: 25,
        endIndent: 25,
      ),
      //   -- > package details
      Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
        child: _packageDetails(globals.newTrip_package),
      ),
      //   -- > price
      Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
          child: _servicePriceEstimate(globals.newTrip_cost)),
      Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: _paymentDetails(globals.whomwillPay)),
    ]);
  }

  void _riderDetailsDialog(String title, String message, BuildContext context) {
    BuildContext dialogContext;
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          dialogContext = context;
          return AlertDialog(
            title: Text(title, style: TextStyle(fontWeight: FontWeight.w400)),
            content:
                _aogroupRider(riderName, riderPhone, motimertocycleNumberPlate),
            actions: <Widget>[],
          );
        });
  }

  @override
  void initState() {
    super.initState();
    if (globals.packageDetailsOK == true && globals.requestedTrip == true) {
      print("== > now${globals.newTrip_dropoff_lat}");
      _fabanimationController = AnimationController(
        vsync: this,
        value: _open ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 250),
      );
      _fabAnimationProgress = CurvedAnimation(
          curve: Curves.fastOutSlowIn,
          reverseCurve: Curves.easeOutQuad,
          parent: _fabanimationController);

      _animationController = AnimationController(
        vsync: this,
        duration: const Duration(seconds: 2),
      );
      _animationController.repeat(reverse: true);
      _animationController.forward();
      // _setLocations();
      setCustomMapPin();
      _pathToBeTravelled();
      periodicApiCall();
      _listenToStream();
      streamSubscription.resume();
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (globals.packageDetailsOK == true && globals.requestedTrip == true) {
      timer.cancel();
      _cancelStreamSubscription();
    }
    _animationController.stop();
    _animationController.dispose();
    _fabanimationController.dispose();
  }

  void _toggle() {
    setState(() {
      _open = !_open;
      if (_open) {
        _fabanimationController.forward();
      } else {
        _openTapToCloseFAB();
        _fabanimationController.reverse();
      }
    });
  }

  _completeTripReq(width, height) {
    if (globals.packageDetailsOK == true && globals.requestedTrip == true) {
      return Container(
          height: height * 0.8,
          width: width,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: new AppBar(
              automaticallyImplyLeading: true,
              backgroundColor: Colors.white,
              title: Row(children: [
                Container(
                  child: SizedBox(
                    height: 40,
                    width: 40,
                    child: FloatingActionButton(
                      elevation: 15,
                      highlightElevation: 15,
                      onPressed: () {
                        globals.pageController.animateToPage(1,
                            duration: Duration(milliseconds: 500),
                            curve: Curves.easeInCubic);
                      },
                      child: const Icon(Icons.arrow_back,
                          color: Colors.white, size: 15),
                      backgroundColor: globals.primaryColor,
                      foregroundColor: globals.accentColor2,
                    ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(20, 0, 0, 00),
                    child: new Text(_title,
                        style: TextStyle(
                            color: globals.accentColor2,
                            fontWeight: FontWeight.w400)))
              ]),
              bottom: PreferredSize(
                  preferredSize: Size(double.infinity, 1.0),
                  child: _progressBar()),
            ),
            body: SlidingUpPanel(
              backdropOpacity: 0.3,
              backdropEnabled: true,
              parallaxEnabled: true,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(18.0),
                  topRight: Radius.circular(18.0)),
              panel: IntrinsicHeight(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Divider(
                      color: Colors.grey[300],
                      height: 30,
                      thickness: 5,
                      indent: 130,
                      endIndent: 130,
                    ),
                    //  -- > estimate time of arrival
                    Container(
                        child: Row(
                      children: <Widget>[
                        Container(child: _currentTime(now)),
                      ],
                    )),
                    Divider(
                      indent: 25,
                      endIndent: 25,
                    ),
                    Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                          // SizedBox(width: 10),
                          Container(
                            // padding: EdgeInsets.fromLTRB(25, 10, 35, 10),
                            padding: EdgeInsets.fromLTRB(25, 5, 35, 5),
                            child: Text(
                              "Order Status : ",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 0, 35, 5),
                              child: Card(
                                  color: statusColor,
                                  elevation: 4,
                                  child: Container(
                                    width: 100,
                                    padding: EdgeInsets.all(5),
                                    child: Text(
                                      tripStatus,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ))),
                          SizedBox(width: 10),
                        ])),
                    // -- > order progress
                    Container(
                      //  margin: const EdgeInsets.fromLTRB(15.0, 7.0, 10.0, 10.0),
                      child: Row(
                        children: <Widget>[
                          Row(
                            children: [
                              //indicators
                              IntrinsicHeight(
                                child: _startDropIcons(),
                              ),
                              // --- > TripStart n drop points
                              Row(
                                children: <Widget>[
                                  Container(
                                      child: _orderProgress(
                                          pickUpAddress, dropOffAddress)),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Card(
                      elevation: 5,
                      color: Colors.grey.shade200,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: _packageSection(),
                    ),
                    (_showLinearProgress == true)
                        ? FloatingActionButton.extended(
                            backgroundColor: globals.accentColor2,
                            onPressed: () {
                              Fluttertoast.showToast(
                                msg: "Rider will be assigned shortly",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                // fontSize: 16.0
                              );
                            },
                            icon: riderIcon,
                            label: Text("Rider will be assigned soon"))
                        : FloatingActionButton.extended(
                            backgroundColor: globals.accentColor,
                            onPressed: () {
                              _toggle();
                              _riderDetailsDialog(
                                  "RIDER DETAILS ", "Welcome", context);
                              // _closeTapToCloseFAB();
                            },
                            icon: riderIcon,
                            label: Text(_assignedRider)),
                  ],
                ),
              ),
              //behind the sliding panel
              body: Stack(
                children: <Widget>[
                  GoogleMap(
                    markers: Set<Marker>.from(markers),
                    initialCameraPosition: _initialLocation,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: false,
                    mapType: MapType.normal,
                    zoomGesturesEnabled: true,
                    zoomControlsEnabled: false,
                    polylines: Set<Polyline>.of(polylines.values),
                    onMapCreated: (GoogleMapController controller) {
                      mapController = controller;
                      mapController
                          .moveCamera(CameraUpdate.newLatLngBounds(bounds, 50));
                    },
                  ),
                  SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ClipOval(
                            child: Material(
                              color: Colors.blue.shade100, // button color
                              child: InkWell(
                                splashColor: Colors.blue, // inkwell color
                                child: SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: Icon(Icons.add),
                                ),
                                onTap: () {
                                  mapController.animateCamera(
                                    CameraUpdate.zoomIn(),
                                  );
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          ClipOval(
                            child: Material(
                              color: Colors.blue.shade100, // button color
                              child: InkWell(
                                splashColor: Colors.blue, // inkwell color
                                child: SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: Icon(Icons.remove),
                                ),
                                onTap: () {
                                  mapController.animateCamera(
                                    CameraUpdate.zoomOut(),
                                  );
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ));
    } else {
      return Container(
          height: height,
          width: width,
          child: Scaffold(
              body: ListView(
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                SizedBox(width: 20),
                SizedBox(
                  height: 40,
                  width: 40,
                  child: FloatingActionButton(
                    elevation: 15,
                    highlightElevation: 15,
                    onPressed: () {
                      globals.pageController.animateToPage(1,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.easeInCubic);
                    },
                    child: const Icon(Icons.arrow_back,
                        color: Colors.white, size: 15),
                    backgroundColor: globals.primaryColor,
                    foregroundColor: globals.accentColor2,
                  ),
                ),

                /* === >  Select Service text < === */
              ]),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 7, 0, 00),
                    child: Text(
                      "Trip Request Status Page .... ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black54,
                          fontSize: 13,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(height: 20),
                  Divider(
                    thickness: 2,
                    color: globals.accentColor,
                    indent: 20,
                    endIndent: 20,
                  ),
                  Container(
                    padding: EdgeInsets.only(),
                    child: Text(
                      'Hi ${globals.name}, Kindly Complete Package Request on Previous Page ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w300,
                        color: globals.primaryColor,
                      ),
                    ),
                  ),
                  Divider(
                    thickness: 2,
                    color: globals.accentColor,
                    indent: 20,
                    endIndent: 20,
                  ),
                  SizedBox(height: 100),
                  Container(
                    child: LoadingBouncingGrid.square(
                      borderColor: Colors.white,
                      borderSize: 3.0,
                      size: 200.0,
                      backgroundColor: globals.primaryColor,
                      duration: Duration(seconds: 120),
                    ),
                  ),
                ],
              ),
            ],
          )));
    }
  }

  @override
  Widget build(BuildContext context) {
    now = estimateDeliveryTime();
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return WillPopScope(
        child: _completeTripReq(width, height),
        onWillPop: () async {
          return false;
        });
  }
}
