import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'dart:ui';

import 'package:flutter/material.dart';

//String BASE_URL = "http://54.93.187.217:8770/channelinterface/courier";
//String BASE_URL = "http://3.70.56.105:8770/channelinterface/courier";
String BASE_URL = "http://18.184.64.21:8770/channelinterface/courier";
//String BASE_URL = "http://aologistics.co.ke:8770/channelinterface/courier";
String username = "courier";
String password = "CoUrier2021";
String channel = "MOBILE";
String phonenumber ="";
String appName = "AO Logistics";

String email = "";
String name = "";
String phone = "";
double rating = 0.0;
String client_type = "";
String corporateName = "";
String paymentBy ="";
String whomwillPay = "";
String promoCode = "";
bool requestedTrip= false;
PageController pageController = PageController();
String supportNumber = "0111036300";
String faqUrl = "http://aologistics.co.ke";
String shareAppLink = "Check out AO LOGISTICS at https://bit.ly/3xY0qfN";

bool packageDetailsOK = false;

int pushID = 1;

getRandom() {
  var rng = new Random();
  for (var i = 0; i < 10; i++) {
    print(rng.nextInt(100));
  }
}

const Color primaryColor = Color(0xff004785);
const Color accentColor = Color(0xffA90533);
const Color accentColor2 = Color(0xff6D6E71);

// current trip varriables
bool newTrip = false;

String newTrip_tripID = "";
String newTrip_date = "";
String newTrip_status = "";
String newTrip_pickup = "";
String newTrip_pickup_lat = "";
String newTrip_pickup_long = "";
String pickUpAddress = "";
String dropOffAddress = "";
String newTrip_dropoff = "";
String newTrip_dropoff_lat = "";
String newTrip_dropoff_long = "";
String newTrip_client = "";
String newTrip_cost = "";
String newTrip_service_payer = "";
String newTrip_package = "";
String newTrip_delivery_type = "";
double newTrip_rider_lat = 0.0;
double newTrip_rider_long = 0.0;
bool newTrip_rider_allocated = false;
String newTrip_recipient_phone = "";
String newTrip_recipient_name = "";
String newTrip_time_estimate = "";
String newTrip_distance = "";

// trip history varriables
String tripDetails_tripID = "";
String tripDetails_date = "";
String tripDetails_status = "";
String tripDetails_pickup = "";
String tripDetails_dropoff = "";
String tripDetails_client = "";
String tripDetails_cost = "";
// String tripDetails_pay_method = "CASH";
String tripDetails_package = "";
String tripDetails_delivery_type = "";

String tripDetails_service_payer = "";
String riderPhone_number ="";


String apiSessionKey = "";
int newPushID()
{
  pushID +=1;
  return pushID;
}

Future<String> apiRequest(String url, Map jsonMap) async {
  String reply = "";
  try{
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    reply = await response.transform(utf8.decoder).join();
    httpClient.close();

  }catch(e) {
    //Handle all other exceptions
    print("apiRequest " + e.toString());
  }

  return reply;

}

Future<bool?> willPopCallback(BuildContext context) async =>
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text("Do you really want to exit the app ?"),
          actions: [
            TextButton(
              child: Text("yes"),
              onPressed: () => Navigator.pop(context, true),
            ),
            TextButton(
              child: Text("no"),
              onPressed: () => Navigator.pop(context, false),
            )
          ],
        ));
