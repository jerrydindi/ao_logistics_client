import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '/routes/pageRoute.dart';
import '../globals.dart' as globals;
import 'package:share/share.dart';

class navigationDrawer extends StatefulWidget {
  _navigationDrawerState createState() => _navigationDrawerState();
}

class _navigationDrawerState extends State<navigationDrawer> {
  late String _username = "";
  late String _useremail = "";
  late var splitEmail = [];
  //TODO: implement user details
  getUserDetails() async {
    SharedPreferences _sharedPrefrences = await SharedPreferences.getInstance();
    globals.name = _sharedPrefrences.getString("userName") ?? "";
    if (_sharedPrefrences.getString("email") != null) {
      setState(() {
        _useremail = globals.email;
        splitEmail = _useremail.split("@");
        _username = globals.name ;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    getUserDetails();

    return Drawer(
      elevation: 10,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: globals.primaryColor),
            accountName: Text(globals.name,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w300)),
            accountEmail: Text(globals.email,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w200)),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                  ? globals.primaryColor
                  : Colors.white,
              child: Text(
                "${globals.name[0]} ",
                style: TextStyle(fontSize: 40.0),
              ),
            ),
          ),
          ListTile(
            title: const Text('Home'),
            leading: Icon(Icons.home_rounded),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.home),
          ),
          ListTile(
            title: const Text('Profile'),
            leading: Icon(Icons.account_circle_rounded),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.profile),
          ),
          ListTile(
            title: const Text('Trips'),
            leading: Icon(Icons.motorcycle_rounded),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.trips),
          ),
          ListTile(
            title: const Text('Promotions'),
            leading: Icon(Icons.card_giftcard_rounded),
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.promotions),
          ),
          Divider(color: Colors.grey),
          ListTile(
            title: const Text('FAQs'),
            leading: Icon(Icons.help_outline_rounded),
            onTap: () {
              launch(globals.faqUrl);
             Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Support'),
            leading: Icon(Icons.call_rounded),
            onTap: () {
              launch("tel://" +globals.supportNumber);
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Share App'),
            leading: Icon(Icons.share_rounded),
            onTap: () {
              Share.share(globals.shareAppLink);
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }


}
