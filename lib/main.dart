import 'package:ao_courier_client/landing.dart';
import 'package:ao_courier_client/otp.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'globals.dart' as globals;

import 'login.dart';
import 'onboarding.dart';

var onBoarded;
var otp;
var login;

getOnBoarded() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  onBoarded = prefs.getString('onBoarded') ?? "";
}

getOtpPersist() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  otp = prefs.getString('otpPersist') ?? "";
}

getLoginPersist() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  login = prefs.getString('loginPersist') ?? "";
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up :  ${message.messageId}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  getOnBoarded();
  getOtpPersist();
  getLoginPersist();

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  //runApp(MyApp());

  runApp(MaterialApp(
      title: 'Introduction screen',
      debugShowCheckedModeBanner: false,
      home: MyHomePage(title: 'Test Tile')));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  void initState() {
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification!.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                color: Colors.transparent,
                playSound: true,
                enableVibration: true,
                largeIcon: DrawableResourceAndroidBitmap( 'ao_logo'),
                ticker: 'ticker',
                sound: RawResourceAndroidNotificationSound('notification_sound'),
                icon: 'ao_logo',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published !');
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification!.android;
      if (notification != null && android != null) {
        showDialog(
            context: context,
            builder: (_) {
              return AlertDialog(
                title: Text(notification.title!),
                content: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [Text(notification.body!)],
                  ),
                ),
              );
            });
      }
    });
  }

  void showNotification() {
    setState(() {
      _counter++;
    });
    flutterLocalNotificationsPlugin.show(
        0,
        "Testing $_counter",
        "How you doin ?",
        NotificationDetails(
            android: AndroidNotificationDetails(
                channel.id, channel.name, channel.description,
                importance: Importance.high,
                color: Colors.blue,
                playSound: true,
                sound: RawResourceAndroidNotificationSound('notification_sound'),
                icon: '@ao_logo')));
  }

  @override
  Widget build(BuildContext context) {
    getOnBoarded();
    getOtpPersist();
    getLoginPersist();

    print("MAIN: Loginpersist: " + login);

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: (onBoarded == null || onBoarded == "")
            ? OnBoardingPage()
            : otp == "1" ? OTPPage() : login == "1" ? LandingPage() : LoginPage() // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
