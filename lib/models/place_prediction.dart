import 'dart:core';

class PlacesPrediction {
  // <fields>
  final String description;
  final String placeID;

// <constructor>
  PlacesPrediction(this.description, this.placeID);

  //Used to construct a new Prediction intance from a map structure
  PlacesPrediction.fromJson(Map<String, dynamic> json)
      : description = json['description'],
        placeID = json['place_id'];

  //convert prediction instance to a map
  Map<String, dynamic> toJson() => {
        'description': description,
        'placeID': placeID,
      };
//  Method can crash if the variable types are not gotten right

}
