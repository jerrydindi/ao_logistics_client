import 'dart:core';
// accountNumber":20014,/"email":"josephridge18@gmail.com","accountType":"CLIENT","corporateCode":"","balance":-207.0
class Accounts {
  // <fields>
  final String accountNumber;
  final String email;
  final String accountType;
  final String corporateCode;
  final double balance;

  Accounts(this.accountNumber, this.email, this.accountType, this.corporateCode,
      this.balance); // <constructor>


  Accounts.fromJson(Map<String, dynamic> json)
      : accountNumber = json['accountNumber'],
        email = json['email'],
        accountType = json['accountType'],
        corporateCode = json['corporateCode'],
        balance = json['balance'];


  Map<String, dynamic> toJson() => {
    'accountNumber': accountNumber,
    'email': email,
    'accountType': accountType,
    'corporateCode': corporateCode,
    'balance': balance
  };
  //Used to construct a new Prediction intance from a map structure
  // Accounts.fromJson(Map<String, dynamic> json)
  //     : description = json['description'],
  //       placeID = json['place_id'];

  //convert prediction instance to a map
  // Map<String, dynamic> toJson() => {
  //   'description': description,
  //   'placeID': placeID,
  // };
//  Method can crash if the variable types are not gotten right

}
